package org.tpc.minecraft.simplekits.commands.admin;

import static org.tpc.minecraft.simplekits.Constants.Messages.*;
import static org.tpc.minecraft.simplekits.Constants.Permissions.*;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.tpc.minecraft.simplekits.IIgnores;
import org.tpc.minecraft.simplekits.IKits;
import org.tpc.minecraft.simplekits.SimpleKits;

public class IgnoreCommand implements CommandExecutor {
	
	private SimpleKits plugin;
	private IKits kits;
	private IIgnores ignores;
	
	public IgnoreCommand(SimpleKits plugin) {
		this.plugin = plugin;
		kits = plugin.getKits();
		ignores = plugin.getIgnores();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		boolean enable = label.equalsIgnoreCase("enable");
		if(!(sender instanceof ConsoleCommandSender)
				&& !sender.hasPermission(enable ? PERMISSION_ADMIN_ENABLE : PERMISSION_ADMIN_DISABLE)) {
			sender.sendMessage(plugin.getMessage(MESSAGE_ERROR_NO_PERMISSION));
			return true;
		}
		
		if(args.length <= 0) {
			return false;
		}
		
		for(String kit : args) {
			if(kits.getKitOrAlias(kit) == null) {
				sender.sendMessage(plugin.getMessage(MESSAGE_ERROR_NO_KIT, args[0]));
				continue;
			}
			
			if(enable) {
				boolean ignored = ignores.isIgnored(kit);
				if(ignored) {
					ignores.remove(kit);
				}
				
				sender.sendMessage(plugin.getMessage(ignored ? MESSAGE_DISPLAY_IGNORE_ENABLE
						: MESSAGE_ERROR_ALREADY_ENABLED, args[0]));
				continue;
			} else {
				boolean ignored = ignores.isIgnored(kit);
				if(!ignored) {
					ignores.store(kit);
				}
				
				sender.sendMessage(plugin.getMessage(ignored ? MESSAGE_ERROR_ALREADY_DISABLED
						: MESSAGE_DISPLAY_IGNORE_DISABLE, args[0]));
				continue;
			}
		}
		
		return true;
	}
}