package org.tpc.minecraft.simplekits.commands;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandsBase {
	
	protected boolean onCommand(CommandExecutor commandExecutor, CommandSender sender, Command command, String label, String[] args) {
		return commandExecutor.onCommand(sender, command, args[0], cutArgs(args));
	}
	
	protected String[] cutArgs(String[] args) {
		return Arrays.copyOfRange(args, Math.min(1, args.length), args.length);
	}
}