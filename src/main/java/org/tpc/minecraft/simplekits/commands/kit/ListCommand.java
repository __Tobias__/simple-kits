package org.tpc.minecraft.simplekits.commands.kit;

import static org.tpc.minecraft.simplekits.Constants.GlobalMessages.*;
import static org.tpc.minecraft.simplekits.Constants.Messages.*;
import static org.tpc.minecraft.simplekits.Constants.Permissions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.tpc.minecraft.simplekits.GlobalMessages;
import org.tpc.minecraft.simplekits.IAliasMap;
import org.tpc.minecraft.simplekits.ICooldowns;
import org.tpc.minecraft.simplekits.IIgnores;
import org.tpc.minecraft.simplekits.IKits;
import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.kit.IKit;

public class ListCommand implements CommandExecutor {
	
	private SimpleKits plugin;
	
	public ListCommand(SimpleKits plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(GlobalMessages.getString(GMESSAGE_COMMAND_ERROR_NO_PLAYER));
			return true;
		}
		
		Player player = (Player) sender;
		
		if(!player.hasPermission(PERMISSION_LIST)) {
			player.sendMessage(plugin.getMessage(MESSAGE_ERROR_NO_PERMISSION));
			return true;
		}
		
		ICooldowns cooldowns = plugin.getCooldowns();
		IIgnores ignores = plugin.getIgnores();
		
		IKits kits = plugin.getKits();
		IAliasMap aliasMap = kits.getAliasMap();
		
		UUID playerID = player.getUniqueId();
		
		List<String> allowedKitList = new ArrayList<>();
		
		for(IKit kit : kits.getKits()) {
			String name = kit.getName();
			if(player.hasPermission(PERMISSION_LIST + '.' + name) && !ignores.isIgnored(name)) {
				allowedKitList.add(kit.getName());
			}
			
			for(String alias : aliasMap.getAliases(name)) {
				if(player.hasPermission(PERMISSION_LIST + '.' + alias)&& !ignores.isIgnored(alias)) {
					allowedKitList.add(alias);
				}
			}
		}
		
		player.sendMessage(plugin.getMessage(MESSAGE_DISPLAY_LIST));
		player.sendMessage(allowedKitList.stream().sorted()
				.map((kit) -> plugin.getMessage(
						(!player.hasPermission(PERMISSION_IGNORE_COOLDOWN + "." + kit) && cooldowns.hasCooldown(kit, playerID))
						? MESSAGE_DISPLAY_LIST_COOLDOWN : MESSAGE_DISPLAY_LIST_AVAILABLE, kit))
				.collect(Collectors.joining(" ")));
		
		return true;
	}
}