package org.tpc.minecraft.simplekits.commands.kit;

import static org.tpc.minecraft.simplekits.Constants.GlobalMessages.*;
import static org.tpc.minecraft.simplekits.Constants.Messages.*;
import static org.tpc.minecraft.simplekits.Constants.Permissions.*;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.tpc.minecraft.simplekits.GlobalMessages;
import org.tpc.minecraft.simplekits.IAliasMap;
import org.tpc.minecraft.simplekits.IIgnores;
import org.tpc.minecraft.simplekits.IKitPlacer;
import org.tpc.minecraft.simplekits.IKits;
import org.tpc.minecraft.simplekits.ILoadable;
import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.kit.IKit;

public class PlaceCommand implements CommandExecutor, ILoadable {
	
	private static final String name(String alias, String name) {
		return (alias != null) ? alias : name;
	}
	
	private SimpleKits plugin;
	
	private IKitPlacer kitPlacer;
	
	private IKits kits;
	private IAliasMap aliasMap;
	private IIgnores ignores;
	
	public PlaceCommand(SimpleKits plugin) {
		this.plugin = plugin;
		this.kitPlacer = plugin.getKitPlacer();
	}
	
	@Override
	public void onLoad() {
		kits = plugin.getKits();
		aliasMap = kits.getAliasMap();
		ignores = plugin.getIgnores();
	}
	
	@Override
	public void onUnload() { }
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(GlobalMessages.getString(GMESSAGE_COMMAND_ERROR_NO_PLAYER));
			return true;
		}
		
		Player player = (Player) sender;
		if(!player.hasPermission(PERMISSION_USE)) {
			player.sendMessage(plugin.getMessage(MESSAGE_ERROR_NO_PERMISSION));
			return true;
		}
		
		if(args.length <= 0) {
			return false; // Print usage
		}
		
		for(String kitName : args) {
			String kitAlias = null;
			if(aliasMap.aliasExists(kitName)) {
				kitName = aliasMap.getObjectForAlias(kitAlias = kitName);
			}
			
			IKit kit;
			if(((kit = kits.getKit(kitName)) == null) || ignores.isIgnored(kitName)) {
				player.sendMessage(plugin.getMessage(MESSAGE_ERROR_NO_KIT, name(kitAlias, kitName)));
				continue;
			}
			
			if(!player.hasPermission(PERMISSION_IGNORE_COOLDOWN) && !player.hasPermission(PERMISSION_USE + "." + kitName)) {
				sender.sendMessage(plugin.getMessage(MESSAGE_ERROR_PLACE_NO_PERMISSION_FOR_KIT, kitName));
				continue;
			}
			
			if(!kitPlacer.place(player.getInventory(), kit, kit, kit, () -> false)) {
				player.sendMessage(plugin.getMessage(MESSAGE_ERROR_PLACE_NO_SPACE, name(kitAlias, kitName)));
				break;
			}
			
			player.sendMessage(plugin.getMessage(MESSAGE_DISPLAY_PLACE_DONE, name(kitAlias, kitName)));
		}
		
		return true;
	}
}