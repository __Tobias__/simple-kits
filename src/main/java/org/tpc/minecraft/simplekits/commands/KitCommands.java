package org.tpc.minecraft.simplekits.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.tpc.minecraft.simplekits.ILoadable;
import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.commands.kit.InfoCommand;
import org.tpc.minecraft.simplekits.commands.kit.ListCommand;
import org.tpc.minecraft.simplekits.commands.kit.PlaceCommand;

public class KitCommands extends CommandsBase implements CommandExecutor, ILoadable {
	
	private InfoCommand infoCommand;
	private PlaceCommand placeCommand;
	private ListCommand listCommand;
	
	public KitCommands(SimpleKits plugin, ListCommand listCommand) {
		infoCommand = new InfoCommand(plugin);
		placeCommand = new PlaceCommand(plugin);
		this.listCommand = listCommand;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length <= 0) {
			return false;
		}
		
		switch(args[0].toLowerCase()) {
		case "info":
			return onCommand(infoCommand, sender, command, label, args);
		case "list":
			return onCommand(listCommand, sender, command, label, args);
		default:
			return placeCommand.onCommand(sender, command, label, args);
		}
	}
	
	
	@Override
	public void onLoad() {
		placeCommand.onLoad();
	}
	
	@Override
	public void onUnload() { }
}