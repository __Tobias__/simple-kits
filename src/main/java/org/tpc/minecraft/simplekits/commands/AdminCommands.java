package org.tpc.minecraft.simplekits.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.commands.admin.IgnoreCommand;
import org.tpc.minecraft.simplekits.commands.admin.ListCommand;

public class AdminCommands extends CommandsBase implements CommandExecutor {
	
	private IgnoreCommand ignoreCommand;
	private ListCommand listCommand;
	
	public AdminCommands(SimpleKits plugin) {
		ignoreCommand = new IgnoreCommand(plugin);
		listCommand = new ListCommand(plugin);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length <= 0) {
			return false;
		}
		
		switch(args[0].toLowerCase()) {
		case "enable":
		case "disable":
			return onCommand(ignoreCommand, sender, command, label, args);
		case "list":
			return onCommand(listCommand, sender, command, label, args);
		default:
			return false;
		}
	}
}