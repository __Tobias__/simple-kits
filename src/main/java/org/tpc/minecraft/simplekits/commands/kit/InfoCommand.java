package org.tpc.minecraft.simplekits.commands.kit;

import static org.tpc.minecraft.simplekits.Constants.Messages.*;
import static org.tpc.minecraft.simplekits.Constants.Permissions.*;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.data.Cooldown;
import org.tpc.minecraft.simplekits.kit.IKit;

import net.time4j.ClockUnit;
import net.time4j.Duration;

public class InfoCommand implements CommandExecutor {
	
private SimpleKits plugin;
	
	public InfoCommand(SimpleKits plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if((sender instanceof Player) && !sender.hasPermission(PERMISSION_LIST)) {
			sender.sendMessage(plugin.getMessage(MESSAGE_ERROR_NO_PERMISSION));
			return true;
		}
		
		if(args.length < 1) {
			sender.sendMessage(plugin.getMessage(MESSAGE_ERROR_NO_PARAMETER_GIVEN));
			return false;
		}
		IKit kit;
		if(plugin.getIgnores().isIgnored(args[0]) || ((kit = plugin.getKits().getKit(args[0])) == null)) {
			sender.sendMessage(plugin.getMessage(MESSAGE_ERROR_NO_KIT, args[0]));
			return false;
		}
		
		if((sender instanceof Player) && !sender.hasPermission(PERMISSION_LIST + "." + args[0])) {
			sender.sendMessage(plugin.getMessage(MESSAGE_ERROR_NO_PERMISSION));
			return true;
		}
		
		String description = kit.getDescription();
		long cooldownTime = kit.getCooldown();
		sender.sendMessage(plugin.getMessage(MESSAGE_DISPLAY_INFO_NAME, args[0]));
		sender.sendMessage(plugin.getMessage(MESSAGE_DISPLAY_INFO_COOLDOWN, (cooldownTime != 0)
				? plugin.getConfiguration().getPeriodFormatter().print(
						Duration.ofZero().plus(Duration.of(cooldownTime, ClockUnit.SECONDS)).with(Duration.approximateSeconds(1)))
						: plugin.getMessage(MESSAGE_DISPLAY_INFO_NO_COOLDOWN, args[0])));
		
		if(sender instanceof Player) {
			Cooldown cooldown = plugin.getCooldowns().get(args[0], ((Player) sender).getUniqueId());
			sender.sendMessage(plugin.getMessage(MESSAGE_DISPLAY_INFO_TIME_REMAINING, (cooldown != null)
					&& ((cooldownTime = (System.currentTimeMillis() - cooldown.getCreationTime() - kit.getCooldown())) > 0)
					? plugin.getConfiguration().getPeriodFormatter().print(
							Duration.ofZero().plus(Duration.of(cooldownTime, ClockUnit.SECONDS)).with(Duration.approximateSeconds(1)))
							: plugin.getMessage(MESSAGE_DISPLAY_INFO_NO_TIME_REMAINING, args[0])));
		}
		
		sender.sendMessage(plugin.getMessage(MESSAGE_DISPLAY_INFO_DESCRIPTION, (description != null)
				? description : plugin.getMessage(MESSAGE_DISPLAY_INFO_NO_DESCRIPTION, args[0])));
		
		return true;
	}
}