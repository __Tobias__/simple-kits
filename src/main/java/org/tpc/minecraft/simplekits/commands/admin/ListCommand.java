package org.tpc.minecraft.simplekits.commands.admin;

import static org.tpc.minecraft.simplekits.Constants.Messages.*;
import static org.tpc.minecraft.simplekits.Constants.Permissions.*;

import java.nio.file.Path;
import java.util.StringJoiner;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.tpc.minecraft.simplekits.IAliasMap;
import org.tpc.minecraft.simplekits.IKits;
import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.kit.IKit;

import net.time4j.ClockUnit;
import net.time4j.Duration;

public class ListCommand implements CommandExecutor {
	
	private SimpleKits plugin;
	
	public ListCommand(SimpleKits plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(!(sender instanceof ConsoleCommandSender)
				&& !sender.hasPermission(PERMISSION_ADMIN_LIST)) {
			sender.sendMessage(plugin.getMessage(MESSAGE_ERROR_NO_PERMISSION));
			return true;
		}
		
		IKits kits = plugin.getKits();
		IAliasMap aliasMap = kits.getAliasMap();
		
		boolean first = true;
		for(IKit kit : kits.getKits()) {
			if(!first) {
				sender.sendMessage("-----------------------------------------------------");
			} else {
				first = false;
			}
			
			StringJoiner joiner = new StringJoiner(" ");
			for(String alias : aliasMap.getAliases(kit.getName())) {
				joiner.add(alias);
			}
			
			String name = kit.getName();
			String aliases = joiner.toString();
			String description = kit.getDescription();
			long cooldown = kit.getCooldown();
			Path source = kit.getSource();
			
			sender.sendMessage(plugin.getMessage(MESSAGE_DISPLAY_ADMIN_INFO_NAME, name));
			
			sender.sendMessage(plugin.getMessage(MESSAGE_DISPLAY_ADMIN_INFO_COOLDOWN, (cooldown != 0)
					? plugin.getConfiguration().getPeriodFormatter().print(
							Duration.ofZero().plus(Duration.of(cooldown, ClockUnit.SECONDS)).with(Duration.approximateSeconds(1)))
							: plugin.getMessage(MESSAGE_DISPLAY_ADMIN_INFO_NO_COOLDOWN, name)));
			sender.sendMessage(plugin.getMessage(MESSAGE_DISPLAY_ADMIN_INFO_ALIASES, aliases.isEmpty()
					? plugin.getMessage(MESSAGE_DISPLAY_ADMIN_INFO_NO_ALIASES, name) : aliases));
			sender.sendMessage(plugin.getMessage(MESSAGE_DISPLAY_ADMIN_INFO_SOURCE, (source != null)
					? source : plugin.getMessage(MESSAGE_DISPLAY_ADMIN_INFO_NO_SOURCE, name)));
			
			sender.sendMessage(plugin.getMessage(MESSAGE_DISPLAY_ADMIN_INFO_DESCRIPTION, (description != null)
					? description : plugin.getMessage(MESSAGE_DISPLAY_ADMIN_INFO_NO_DESCRIPTION, name)));
		}
		
		return true;
	}
}