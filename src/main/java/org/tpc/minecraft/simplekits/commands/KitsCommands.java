package org.tpc.minecraft.simplekits.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.tpc.minecraft.simplekits.commands.kit.ListCommand;

public class KitsCommands implements CommandExecutor {
	
	private ListCommand listCommand;
	
	public KitsCommands(ListCommand listCommand) {
		this.listCommand = listCommand;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		return listCommand.onCommand(sender, command, label, args);
	}
}