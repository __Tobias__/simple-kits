package org.tpc.minecraft.simplekits;

import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

public class Constants {
	
	public static class DataModel {
		
		public static final String KIT_NAME = "kit"; //$NON-NLS-1$
		public static final String KIT_ALIAS_NAME = "alias"; //$NON-NLS-1$
		public static final String KIT_ALIASES_NAME = "aliases"; //$NON-NLS-1$
		public static final String KIT_BASE_NAME = "base"; //$NON-NLS-1$
		public static final String KIT_BASES_NAME = "bases"; //$NON-NLS-1$
		public static final String KIT_ITEM_NAME = "item"; //$NON-NLS-1$
		public static final String KIT_ITEMS_NAME = "items"; //$NON-NLS-1$
		public static final String KIT_COOLDOWN_NAME = "cooldown"; //$NON-NLS-1$
		
		public static final String REFERENCE_NAME = "reference"; //$NON-NLS-1$
		public static final String REFERENCE_TYPE_NAME = "type"; //$NON-NLS-1$
		
		public static final String REFERENCE_REFERENCE_NAME = "reference"; //$NON-NLS-1$
		public static final String REFERENCE_KIT_NAME = "kit"; //$NON-NLS-1$
		public static final String REFERENCE_TEMPLATE_NAME = "template"; //$NON-NLS-1$
		public static final String[] REFERENCE_NAMES = {
				REFERENCE_REFERENCE_NAME,
				"ref" //$NON-NLS-1$
			};
		
		public static final String POTION_EFFECT_NAME = "effect"; //$NON-NLS-1$
		public static final String POTION_EFFECTS_NAME = "effects"; //$NON-NLS-1$
		
		public static final String BASE_NAME_NAME = "name"; //$NON-NLS-1$
		public static final String BASE_PRIORITY_NAME = "priority"; //$NON-NLS-1$
		
		public static final String BASE_TYPE_NAME = "type"; //$NON-NLS-1$
		
		public static final String BASE_TYPE_REFERENCE_NAME = "reference"; //$NON-NLS-1$
		public static final String BASE_TYPE_KIT_NAME = "kit"; //$NON-NLS-1$
		public static final String BASE_TYPE_TEMPLATE_NAME = "template"; //$NON-NLS-1$
		public static final String[] BASE_TYPE_NAMES = {
				BASE_TYPE_REFERENCE_NAME,
				"ref" //$NON-NLS-1$
			};
		
		public static final String USE_NAME_NAME = "name"; //$NON-NLS-1$
		public static final String USE_IGNORE_SLOTS_NAME = "ignoreSlots"; //$NON-NLS-1$
		
		public static final String ENCHANTMENT_TYPE_NAME = "type"; //$NON-NLS-1$
		public static final String ENCHANTMENT_LEVEL_NAME = "level"; //$NON-NLS-1$
		public static final String ENCHANTMENT_NAME_NAME = "name"; //$NON-NLS-1$
		
		public static final String ITEM_ID_NAME = "id"; //$NON-NLS-1$
		public static final String ITEM_AMOUNT_NAME = "amount"; //$NON-NLS-1$
		public static final String ITEM_SLOT_NAME = "slot"; //$NON-NLS-1$
		public static final String ITEM_DAMAGE_NAME = "damage"; //$NON-NLS-1$
		
		public static final String ITEM_NAME_NAME = "name"; //$NON-NLS-1$
		public static final String ITEM_ENCHANTMENT_NAME = "enchantment"; //$NON-NLS-1$
		public static final String ITEM_ENCHANTMENTS_NAME = "enchantments"; //$NON-NLS-1$
		
		public static final String POTION_EFFECT_TYPE_NAME = "type"; //$NON-NLS-1$
		public static final String POTION_EFFECT_AMPLIFIER_NAME = "amplifier"; //$NON-NLS-1$
		public static final String POTION_EFFECT_DURATION_NAME = "duration"; //$NON-NLS-1$
		
		public static final String POTION_EFFECT_SPLASH_NAME = "splash"; //$NON-NLS-1$
		public static final String POTION_EFFECT_EXTENDED_NAME = "extended"; //$NON-NLS-1$
		
	}
	
	public static class Configuration {
		
		public static final String CONFIG_PATHS_KEY = "paths.base"; //$NON-NLS-1$
		public static final String CONFIG_KITS_PATHS_KEY = "paths.kits"; //$NON-NLS-1$
		public static final String CONFIG_TEMPLATES_PATHS_KEY = "paths.templates"; //$NON-NLS-1$
		public static final String CONFIG_IGNORES_KEY = "paths.ignores"; //$NON-NLS-1$
		public static final String CONFIG_HIDDEN_KEY = "file.hides"; //$NON-NLS-1$
		
		public static final String CONFIG_KEY_LOCALE = "locale"; //$NON-NLS-1$
		public static final String CONFIG_DEFAULT_LOCALE = "en-US"; //$NON-NLS-1$
		
		public static final String CONFIG_KEY_MESSAGES = "messages"; //$NON-NLS-1$
		public static final String CONFIG_DEFAULT_MESSAGES = "messages.properties"; //$NON-NLS-1$
		
		public static final String CONFIG_NAME_KIT_PREFIX_KEY = "name.kit.prefix"; //$NON-NLS-1$
		public static final String CONFIG_NAME_KIT_SUFFIX_KEY = "name.kit.suffix"; //$NON-NLS-1$
		public static final String CONFIG_NAME_TEMPLATE_PREFIX_KEY = "name.template.prefix"; //$NON-NLS-1$
		public static final String CONFIG_NAME_TEMPLATE_SUFFIX_KEY = "name.template.suffix"; //$NON-NLS-1$
		public static final String CONFIG_NAME_SLOTS = "name.slots"; //$NON-NLS-1$
		public static final String CONFIG_NAME_MATERIALS = "name.materials"; //$NON-NLS-1$
		public static final String CONFIG_NAME_AMOUNTS = "name.amounts"; //$NON-NLS-1$
		public static final String CONFIG_NAME_ENCHANTMENTS = "name.enchantments"; //$NON-NLS-1$
		
		public static final String CONFIG_FEATURES = "features"; //$NON-NLS-1$
		public static final String CONFIG_FEATURE_STACKS = "stack-amounts"; //$NON-NLS-1$
		public static final String CONFIG_FEATURE_STACKS_ENABLE = "enable"; //$NON-NLS-1$
		
	}
	
	public static class DefaultResources {
		
		public static final String SLOTS_FILE_NAME = "def/slots.json"; //$NON-NLS-1$
		public static final String MATERIALS_FILE_NAME = "def/materials.json"; //$NON-NLS-1$
		public static final String AMOUNTS_FILE_NAME = "def/amounts.json"; //$NON-NLS-1$
		public static final String ENCHANTMENTS_FILE_NAME = "def/enchantments.json"; //$NON-NLS-1$
		
		public static final String DEFAULTS_PREFIX = "defaults/"; //$NON-NLS-1$
		public static final String DEFAULTS_LIST = DEFAULTS_PREFIX + "defaults.copy"; //$NON-NLS-1$
		public static final String DEFAULTS_SLOTS = DEFAULTS_PREFIX + SLOTS_FILE_NAME;
		public static final String DEFAULTS_MATERIALS = DEFAULTS_PREFIX + MATERIALS_FILE_NAME;
		public static final String DEFAULTS_AMOUNTS = DEFAULTS_PREFIX + AMOUNTS_FILE_NAME;
		public static final String DEFAULTS_ENCHANTMENTS = DEFAULTS_PREFIX + ENCHANTMENTS_FILE_NAME;
		
	}
	
	public static class Permissions {
		
		public static final String PERMISSION_BASE = "simplekits"; //$NON-NLS-1$
		public static final String PERMISSION_ADMIN_BASE = PERMISSION_BASE + ".admin"; //$NON-NLS-1$
		
		public static final String PERMISSION_LIST = PERMISSION_BASE + ".list"; //$NON-NLS-1$
		public static final String PERMISSION_USE = PERMISSION_BASE + ".use"; //$NON-NLS-1$
		public static final String PERMISSION_INFO = PERMISSION_BASE + ".info"; //$NON-NLS-1$
		public static final String PERMISSION_IGNORE_COOLDOWN = PERMISSION_BASE + ".ignore-cooldown"; //$NON-NLS-1$
		
		public static final String PERMISSION_ADMIN_CREATE = PERMISSION_ADMIN_BASE + ".create"; //$NON-NLS-1$
		public static final String PERMISSION_ADMIN_LIST = PERMISSION_ADMIN_BASE + ".list"; //$NON-NLS-1$
		public static final String PERMISSION_ADMIN_ENABLE = PERMISSION_ADMIN_BASE + ".enable"; //$NON-NLS-1$
		public static final String PERMISSION_ADMIN_DISABLE = PERMISSION_ADMIN_BASE + ".disable"; //$NON-NLS-1$
		
		public static final Permission[] PERMISSIONS = {
				new Permission(PERMISSION_LIST, PermissionDefault.TRUE),
				new Permission(PERMISSION_INFO, PermissionDefault.TRUE),
				new Permission(PERMISSION_USE, PermissionDefault.OP),
				new Permission(PERMISSION_IGNORE_COOLDOWN, PermissionDefault.FALSE),
				
				new Permission(PERMISSION_ADMIN_ENABLE, PermissionDefault.FALSE),
				new Permission(PERMISSION_ADMIN_DISABLE, PermissionDefault.FALSE)
		};
	}
	
	public static class GlobalMessages {
		
		public static final String GMESSAGE_CONFIG_INFO_PLUGIN_FOLDER = "configuration.info.plugin-folder"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_INFO_KIT_PATH_ADDED = "configuration.info.kit-path-added"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_INFO_TEMPLATE_PATH_ADDED = "configuration.info.template-path-added"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_INFO_IGNORE_ADDED = "configuration.info.ignore-added"; //$NON-NLS-1$
		
		public static final String GMESSAGE_CONFIG_INFO_TEMPLATES_LOADED = "configuration.info.loaded-templates"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_INFO_TEMPLATE_LOADED = "configuration.info.loaded-template"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_INFO_KITS_LOADED = "configuration.info.loaded-kits"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_INFO_KIT_LOADED = "configuration.info.loaded-kit"; //$NON-NLS-1$
		
		public static final String GMESSAGE_CONFIG_ERROR_INVALID_CONFIG_PATH = "configuration.error.invalid-config-path"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_PATH_IGNORED = "configuration.error.kit-path-ignored"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_NO_KIT_PATHS_DEFINED = "configuration.error.no-kit-paths-defined"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_TEMPLATE_PATH_IGNORED = "configuration.error.template-path-ignored"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_NO_TEMPLATE_PATHS_DEFINED = "configuration.error.no-template-paths-defined"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_IGNORE_INVALID = "configuration.error.ignore-invalid"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_INVALID_MESSAGES = "configuration.error.invalid-messages"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_NO_PATH_SEPARATOR_IN_SUFFIX = "configuration.error.no-path-separator-in-suffix"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_NO_PATH_SEPARATOR_IN_PREFIX = "configuration.error.no-path-separator-in-prefix"; //$NON-NLS-1$
		
		public static final String GMESSAGE_CONFIG_ERROR_KIT_ACCESS = "kits.error.kit.access"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_LOAD = "kits.error.kit.load"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_FOLDER_ACCESS = "kits.error.kit.folder.access"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_KIT_TEMPLATE_EXISTS = "kits.error.kit.kit.template.exists"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_KIT_KIT_EXISTS = "kits.error.kit.kit.kit.exists"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_KIT_ALIAS_TEMPLATE_EXISTS = "kits.error.kit.kit.alias.template.exists"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_KIT_ALIAS_KIT_EXISTS = "kits.error.kit.kit.alias.kit.exists"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_ALIAS_TEMPLATE_EXISTS = "kits.error.kit.alias.template.exists"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_ALIAS_KIT_EXISTS = "kits.error.kit.alias.kit.exists"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_ALIAS_ALIAS_TEMPLATE_EXISTS = "kits.error.kit.alias.alias.template.exists"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_ALIAS_ALIAS_KIT_EXISTS = "kits.error.kit.alias.alias.kit.exists"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_TEMPLATE_ACCESS = "kits.error.template.access"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_TEMPLATE_LOAD = "kits.error.template.load"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_TEMPLATE_FOLDER_ACCESS = "kits.error.template.folder.access"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_TEMPLATE_ALIAS_ALIAS_EXISTS = "kits.error.template.alias.alias.exists"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_TEMPLATE_ALIAS_TEMPLATE_EXISTS = "kits.error.template.alias.template.exists"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_TEMPLATE_TEMPLATE_TEMPLATE_EXISTS = "kits.error.template.template.template.exists"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_TEMPLATE_TEMPLATE_ALIAS_EXISTS = "kits.error.template.template.alias.exists"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_TEMPLATE_REFERENCE_NOT_FOUND_BASE = "kits.error.template.reference.not-found.base"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_TEMPLATE_REFERENCE_NOT_FOUND_REF = "kits.error.template.reference.not-found.ref"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_TEMPLATE_REFERENCE_CIRCULAR_BASE = "kits.error.template.reference.circular.base"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_TEMPLATE_REFERENCE_CIRCULAR_REF = "kits.error.template.reference.circular.ref"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_REFERENCE_NOT_FOUND_BASE = "kits.error.kit.reference.not-found.base"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_REFERENCE_NOT_FOUND_REF = "kits.error.kit.reference.not-found.ref"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_REFERENCE_CIRCULAR_BASE = "kits.error.kit.reference.circular.base"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_REFERENCE_CIRCULAR_REF = "kits.error.kit.reference.circular.ref"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_REFERENCE_IN_TEMPLATE_BASE = "kits.error.template.kit-reference.base"; //$NON-NLS-1$
		public static final String GMESSAGE_CONFIG_ERROR_KIT_REFERENCE_IN_TEMPLATE_REF = "kits.error.template.kit-reference.ref"; //$NON-NLS-1$
		
		public static final String GMESSAGE_MESSAGES_ERROR_MESSAGE_EXTRACT_FAILED = "messages.error.extract-failed"; //$NON-NLS-1$
		public static final String GMESSAGE_MESSAGES_ERROR_MESSAGE_LOAD_FAILED = "messages.error.load-failed"; //$NON-NLS-1$
		
		public static final String GMESSAGE_COMMAND_ERROR_NO_PLAYER = "command.error.no-player"; //$NON-NLS-1$
		
		public static final String GMESSAGE_DB_ERROR_NOT_FOUND = "database.error.not-found"; //$NON-NLS-1$
		public static final String GMESSAGE_DB_ERROR_INIT_FAILED = "database.error.init-failed"; //$NON-NLS-1$
	}
	
	public static class Messages {
		
		public static final String MESSAGE_ERROR_NO_PERMISSION = "error.no-permission"; //$NON-NLS-1$
		public static final String MESSAGE_ERROR_NO_PARAMETER_GIVEN = "error.no-parameter-given"; //$NON-NLS-1$
		public static final String MESSAGE_ERROR_NO_KIT = "error.no-kit"; //$NON-NLS-1$
		
		public static final String MESSAGE_DISPLAY_LIST = "list.display"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_LIST_AVAILABLE = "list.display.available"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_LIST_COOLDOWN = "list.display.cooldown"; //$NON-NLS-1$
		
		public static final String MESSAGE_DISPLAY_INFO_NAME = "info.display.name"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_INFO_COOLDOWN = "info.display.cooldown"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_INFO_DESCRIPTION = "info.display.description"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_INFO_TIME_REMAINING = "info.display.time-remaining"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_INFO_NO_COOLDOWN = "info.display.no-cooldown"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_INFO_NO_DESCRIPTION = "info.display.no-description"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_INFO_NO_TIME_REMAINING = "info.display.no-time-remaining"; //$NON-NLS-1$
		
		public static final String MESSAGE_ERROR_PLACE_NO_SPACE = "place.error.no-space"; //$NON-NLS-1$
		public static final String MESSAGE_ERROR_PLACE_NO_PERMISSION_FOR_KIT = "place.error.no-permission"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_PLACE_DONE = "place.display.done"; //$NON-NLS-1$
		
		public static final String MESSAGE_DISPLAY_IGNORE_ENABLE = "ignore.display.enable"; //$NON-NLS-1$
		public static final String MESSAGE_ERROR_ALREADY_ENABLED = "ignore.error.already-enabled"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_IGNORE_DISABLE = "ignore.display.disable"; //$NON-NLS-1$
		public static final String MESSAGE_ERROR_ALREADY_DISABLED = "ignore.error.already-disabled"; //$NON-NLS-1$
		
		public static final String MESSAGE_DISPLAY_ADMIN_INFO_NAME = "info.display.admin.name"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_ADMIN_INFO_COOLDOWN = "info.display.admin.cooldown"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_ADMIN_INFO_DESCRIPTION = "info.display.admin.description"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_ADMIN_INFO_SOURCE = "info.display.admin.source"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_ADMIN_INFO_ALIASES = "info.display.admin.aliases"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_ADMIN_INFO_NO_COOLDOWN = "info.display.admin.no-cooldown"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_ADMIN_INFO_NO_DESCRIPTION = "info.display.admin.no-description"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_ADMIN_INFO_NO_SOURCE = "info.display.admin.no-source"; //$NON-NLS-1$
		public static final String MESSAGE_DISPLAY_ADMIN_INFO_NO_ALIASES = "info.display.admin.no-aliases"; //$NON-NLS-1$
		
	}
	
	public static class Defaults {
		
		public static final String COMMAND_PREFIX = "simplekits";
		
		public static final boolean REFERENCE_IGNORE_SLOTS = false; // References don't ignore slots by default
		public static final boolean FEATURE_STACKS_ENABLE = true; // Activate stack-sizes
		
		public static final long AMOUNTS_MAX_FRACTIAL_AMOUNTS = 5;
		public static final long AMOUNTS_MAX_STACKS = 100;
	}
}