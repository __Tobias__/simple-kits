package org.tpc.minecraft.simplekits;

import java.util.Map;

import org.tpc.minecraft.simplekits.kit.IKit;

public interface IKits {
	
	public IAliasMap getAliasMap();
	
	public IKit getKit(String name);
	public Iterable<IKit> getKits();
	public boolean kitExists(String name);
	
	public IKit getKitOrAlias(String name);
	public boolean kitOrAliasExists(String name);
	
	public Iterable<Map.Entry<String, IKit>> getNamedKits();
	public Iterable<Map.Entry<String, IKit>> getNamedAliases();
	public Iterable<Map.Entry<String, IKit>> getNamedKitsOrAliases();
	
}