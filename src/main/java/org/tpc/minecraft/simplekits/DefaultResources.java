package org.tpc.minecraft.simplekits;

import static org.tpc.minecraft.simplekits.Constants.DefaultResources.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.io.IOUtils;

public class DefaultResources {
	
	private SimpleKits plugin;
	
	public DefaultResources(SimpleKits plugin) {
		this.plugin = plugin;
	}
	
	public void extract(Path configPath) {
		List<String> lines;
		try(InputStream in = plugin.getResource(DEFAULTS_LIST)) {
			lines = IOUtils.readLines(in, StandardCharsets.UTF_8);
		} catch (IOException | NullPointerException e) {
			plugin.getLogger().log(Level.WARNING, "Could not load default file list!");
			return;
		}
		
		for(String line : lines) {
			String path;
			boolean onlyNew;
			
			if((line = line.trim()).startsWith("#") || line.isEmpty()) {
				continue;
			}
			
			if(onlyNew = line.startsWith("!")) {
				line = line.substring(1).trim();
			}
			
			int index = line.indexOf("->");
			if(index == -1) {
				
				
				if(line.startsWith(DEFAULTS_PREFIX)) {
					path = line.substring(DEFAULTS_PREFIX.length());
				} else {
					path = line;
				}
			} else {
				path = line.substring(index + 2).trim();
				line = line.substring(0, index).trim();
			}
			
			copy(line, path, onlyNew, configPath);
		}
	}
	
	private void copy(String source, String destination, boolean onlyNew, Path configPath) {
		try {
			Path destPath = Paths.get(destination).normalize();
			Path destParentPath = destPath.getParent();
			if(onlyNew && ((destParentPath == null) || Files.exists(configPath.resolve(destParentPath)))) {
				return;
			}
			
			destPath = configPath.resolve(destination);
			destParentPath = destPath.getParent();
			if(destParentPath != null) {
				Files.createDirectories(destParentPath);
			}
			
			try(OutputStream out = Files.newOutputStream(destPath, StandardOpenOption.CREATE_NEW);
					InputStream in = plugin.getResource(source)) {
				IOUtils.copyLarge(in, out);
			} catch (FileAlreadyExistsException e) {
				// Do not overwrite existing file
			} catch (IOException | NullPointerException e) {
				plugin.getLogger().log(Level.WARNING, "Could not copy default file: '" + destination + "'");
			}
		} catch(InvalidPathException | IOException e) {
			plugin.getLogger().log(Level.WARNING, "Could not copy default file: '" + destination + "'");
		}
	}
}