package org.tpc.minecraft.simplekits;

import static org.tpc.minecraft.simplekits.Constants.GlobalMessages.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class Messages {
	
	private SimpleKits plugin;
	private ResourceBundle fallback;
	private ResourceBundle resources;
	
	public Messages(SimpleKits plugin) {
		this.plugin = plugin;
	}
	
	public void load(Path path, Locale locale) {
		fallback = ResourceBundle.getBundle("defaults/messages/messages", locale, plugin.getClass().getClassLoader());
		
		if(!Files.exists(path)) {
			try(BufferedWriter bufferedWriter = Files.newBufferedWriter(path)) {
				Enumeration<String> keys = fallback.getKeys();
				while(keys.hasMoreElements()) {
					String key = keys.nextElement();
					
					bufferedWriter.write(key);
					bufferedWriter.write("=");
					bufferedWriter.write(fallback.getString(key));
					bufferedWriter.newLine();
				}
			} catch(IOException ioException) {
				plugin.getLogger().log(Level.WARNING, GlobalMessages.getString(
						GMESSAGE_MESSAGES_ERROR_MESSAGE_EXTRACT_FAILED), ioException);
			}
		}
		
		if(Files.isRegularFile(path)) {
			try(BufferedReader in = Files.newBufferedReader(path)) {
				resources = new PropertyResourceBundle(in);
			} catch (IOException ioException) {
				plugin.getLogger().log(Level.WARNING, GlobalMessages.getString(
						GMESSAGE_MESSAGES_ERROR_MESSAGE_LOAD_FAILED, path), ioException);
			}
		}
	}
	
	public String getMessage(String name, Object ... parameter) {
		if(resources == null) {
			if(fallback == null) {
				return "!" + name + "!";
			}
			
			try {
				return MessageFormat.format(fallback.getString(name), parameter);
			} catch(MissingResourceException internalResourceException) {
				return "!" + name + "!";
			}
		}
		
		try {
			return MessageFormat.format(resources.getString(name), parameter);
		} catch(MissingResourceException resourceException) {
			if(fallback == null) {
				return "!" + name + "!";
			}
			
			try {
				return MessageFormat.format(fallback.getString(name), parameter);
			} catch(MissingResourceException internalResourceException) {
				return "!" + name + "!";
			}
		}
	}
}