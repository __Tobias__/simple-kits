package org.tpc.minecraft.simplekits.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.tpc.minecraft.simplekits.IAliasMap;
import org.tpc.minecraft.simplekits.IKits;
import org.tpc.minecraft.simplekits.kit.IKit;

public class Kits implements IKits {
	
	private AliasMap aliasMap;
	
	private Map<String, IKit> kits;
	private Map<String, IKit> aliases;
	
	protected Kits() {
		kits = new TreeMap<>();
		aliasMap = new AliasMap();
		aliases = new TreeMap<>();
	}
	
	protected Kits(Map<String, IKit> kits, AliasMap aliasMap) {
		this.kits = kits;
		this.aliasMap = aliasMap;
		aliases = new TreeMap<>();
		
		for(String alias : aliasMap.getAliases()){
			aliases.put(alias, kits.get(aliasMap.getObjectForAlias(alias)));
		}
	}
	
	public void addKit(String name, IKit kit) {
		kits.put(name, kit);
	}
	
	public void addAlias(String name, IKit kit) {
		aliasMap.addAlias(name, kit.getName());
		aliases.put(name, kit);
	}
	
	@Override
	public IKit getKit(String name) {
		return kits.get(name);
	}
	
	@Override
	public boolean kitExists(String name) {
		return kits.containsKey(name);
	}
	
	@Override
	public Iterable<IKit> getKits() {
		return Collections.unmodifiableCollection(kits.values());
	}
	
	@Override
	public IAliasMap getAliasMap() {
		return aliasMap;
	}
	
	@Override
	public IKit getKitOrAlias(String name) {
		if(aliases.containsKey(name)) {
			return aliases.get(name);
		}
		
		return kits.get(name);
	}
	
	@Override
	public boolean kitOrAliasExists(String name) {
		return aliases.containsKey(name) || kits.containsKey(name);
	}
	
	@Override
	public Iterable<Entry<String, IKit>> getNamedKits() {
		return Collections.unmodifiableSet(kits.entrySet());
	}
	
	@Override
	public Iterable<Entry<String, IKit>> getNamedAliases() {
		return Collections.unmodifiableSet(aliases.entrySet());
	}
	
	@Override
	public Iterable<Entry<String, IKit>> getNamedKitsOrAliases() {
		Set<Entry<String, IKit>> set = new HashSet<>();
		
		set.addAll(aliases.entrySet());
		set.addAll(kits.entrySet());
		
		return Collections.unmodifiableSet(set);
	}
}