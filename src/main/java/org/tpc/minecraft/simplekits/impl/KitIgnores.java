package org.tpc.minecraft.simplekits.impl;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.PersistenceException;

import org.tpc.minecraft.simplekits.IIgnores;
import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.data.Ignore;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.ExpressionFactory;

public class KitIgnores implements IIgnores {
	
	private SimpleKits plugin;
	
	private Logger logger;
	private EbeanServer database;
	
	public KitIgnores(SimpleKits plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public void onLoad() {
		logger = plugin.getLogger();
		database = plugin.getDatabase();
	}
	
	@Override
	public void onUnload() { }
	
	@Override
	public void store(String kit) {
		try {
			ExpressionFactory factory = database.getExpressionFactory();
			Ignore ignoreInDB = database.find(Ignore.class).select("kit")
					.where(factory.eq("kit", kit)).findUnique();
			if(ignoreInDB == null) {
				database.insert(new Ignore(kit));
			}
		} catch(PersistenceException e) {
			logger.log(Level.WARNING, String.format("Could not save ignore of %s in database!", kit));
		}
	}
	
	@Override
	public boolean isIgnored(String kit) {
		try {
			ExpressionFactory factory = database.getExpressionFactory();
			
			return (database.find(Ignore.class).select("kit")
					.where(factory.eq("kit", kit)).findUnique() != null);
		} catch(PersistenceException e) {
			logger.log(Level.WARNING, "Could not retrieve ignore from database!", e);
		}
		
		return false;
	}
	
	@Override
	public void remove(String kit) {
		try {
			ExpressionFactory factory = database.getExpressionFactory();
			List<?> ids = database.createQuery(Ignore.class).select("kit").where(
					factory.eq("kit", kit)).findIds();
			
			database.delete(Ignore.class, ids);
		} catch(PersistenceException e) {
			logger.log(Level.WARNING, "Could not delete ignore in database!", e);
		}
	}
}