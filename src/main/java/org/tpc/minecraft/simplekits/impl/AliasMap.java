package org.tpc.minecraft.simplekits.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import org.tpc.minecraft.simplekits.IAliasMap;

public class AliasMap implements IAliasMap {
	
	protected Map<String, String> aliasToObject;
	protected Map<String, Collection<String>> objectToAliases;
	
	public AliasMap() {
		aliasToObject = new TreeMap<>();
		objectToAliases = new TreeMap<>();
	}
	
	@Override
	public boolean aliasExists(String alias) {
		return aliasToObject.containsKey(alias);
	}
	
	@Override
	public String getObjectForAlias(String alias) {
		return aliasToObject.get(alias);
	}
	
	@Override
	public Iterable<String> getAliases(String object) {
		if(objectToAliases.containsKey(object)) {
			return Collections.unmodifiableCollection(objectToAliases.get(object));
		}
		
		return Collections.emptySet();
	}
	
	@Override
	public Iterable<String> getAliases() {
		return Collections.unmodifiableSet(aliasToObject.keySet());
	}
	
	public void addAlias(String name, String alias) {
		aliasToObject.put(alias, name);
		
		Collection<String> aliases;
		if(objectToAliases.containsKey(name)) {
			aliases = objectToAliases.get(name);
		} else {
			aliases = new ArrayList<>();
		}
		
		aliases.add(alias);
	}
	
	public void removeAlias(String name) {
		String object = aliasToObject.remove(name);
		if(object == null) {
			return;
		}
		
		Collection<String> aliases = objectToAliases.get(object);
		if(aliases == null) {
			return;
		}
		
		aliases.remove(name);
	}
	
	public void removeObject(String name) {
		Collection<String> aliases = objectToAliases.remove(name);
		if(aliases == null) {
			return;
		}
		
		for(String alias : aliases) {
			aliasToObject.remove(alias);
		}
	}
}