package org.tpc.minecraft.simplekits.impl;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.PersistenceException;

import org.apache.commons.io.IOUtils;
import org.tpc.minecraft.simplekits.ICooldowns;
import org.tpc.minecraft.simplekits.IKits;
import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.data.Cooldown;
import org.tpc.minecraft.simplekits.kit.IKit;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.ExpressionFactory;
import com.avaje.ebean.QueryIterator;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

public class KitCooldowns implements ICooldowns {
	
	private SimpleKits plugin;
	private IKits kits;
	private Logger logger;
	
	private EbeanServer database;
	
	private ScheduledExecutorService cleanupExecutor;
	private long clearTime;
	
	public KitCooldowns(SimpleKits plugin) {
		this.plugin = plugin;
		logger = plugin.getLogger();
		clearTime = 5 * 3600 * 1000;
	}
	
	@Override
	public void onLoad() {
		kits = plugin.getKits();
		database = plugin.getDatabase();
		
		cleanupExecutor = Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder().setDaemon(true).build());
		cleanupExecutor.scheduleAtFixedRate(this::clean, clearTime, clearTime, TimeUnit.MILLISECONDS);
	}
	
	@Override
	public void onUnload() {
		ExecutorService executorService = cleanupExecutor;
		if((executorService != null) && (!executorService.isShutdown())) {
			executorService.shutdownNow();
		}
	}
	
	protected void clean() {
		long startTime = System.currentTimeMillis();
		int deleted = 0;
		
		logger.log(Level.FINE, "Clearing out old messages ...");
		
		QueryIterator<Cooldown> iterator = null;
		try {
			iterator = database.createQuery(Cooldown.class).select("id,kitName,creationTime").findIterate();
			
			while(iterator.hasNext()) {
				Cooldown cooldown = iterator.next();
				
				String kitName;
				IKit kit;
				if((cooldown == null) || ((kitName = cooldown.getKitName()) == null) || kitName.isEmpty()
						|| ((kit = kits.getKitOrAlias(kitName)) == null)
						|| (cooldown.getCreationTime() + kit.getCooldown() > System.currentTimeMillis())) {
					database.delete(cooldown);
					deleted++;
				}
			}
		} catch(PersistenceException e) {
			logger.log(Level.WARNING, "Could not clean up cooldowns in database!", e);
		} finally {
			IOUtils.closeQuietly(iterator);
		}
		
		logger.log(Level.FINE, String.format("Cleared %d cooldowns in %d ms!", deleted, (System.currentTimeMillis() - startTime)));
	}
	
	public void store(String kit, UUID player) {
		try {
			ExpressionFactory factory = database.getExpressionFactory();
			Cooldown cooldownInDB = database.find(Cooldown.class)
					.where(factory.and(factory.eq("player", player), factory.eq("kitName", kit))).findUnique();
			if(cooldownInDB != null) {
				cooldownInDB.setCreationTime(System.currentTimeMillis());
				database.save(cooldownInDB);
			} else {
				database.insert(new Cooldown(player, kit));
			}
		} catch(PersistenceException e) {
			logger.log(Level.WARNING, String.format("Could not save cooldown of %s for player %s in database!", kit, player));
		}
	}
	
	public Cooldown get(String kit, UUID player) {
		try {
			ExpressionFactory factory = database.getExpressionFactory();
			return database.find(Cooldown.class)
					.where(factory.and(factory.eq("player", player), factory.eq("kitName", kit))).findUnique();
		} catch(PersistenceException e) {
			logger.log(Level.WARNING, "Could not retrieve cooldown from database!", e);
		}
		
		return null;
	}
	
	public boolean hasCooldown(String kitName, UUID player) {
		try {
			ExpressionFactory factory = database.getExpressionFactory();
			IKit kit = kits.getKit(kitName);
			if(kit == null) {
				return false;
			}
			
			Cooldown cooldown = database.find(Cooldown.class)
					.where(factory.and(factory.eq("player", player), factory.eq("kitName", kitName))).findUnique();
			if(cooldown == null) {
				return false;
			}
			
			return (cooldown.getCreationTime() + kit.getCooldown() <= System.currentTimeMillis());
		} catch(PersistenceException e) {
			logger.log(Level.WARNING, "Could not retrieve cooldown from database!", e);
		}
		
		return false;
	}
	
	public void remove(String kit, UUID player) {
		try {
			ExpressionFactory factory = database.getExpressionFactory();
			List<?> ids = database.createQuery(Cooldown.class).select("player,kitName").where(
					factory.and(factory.eq("player", player), factory.eq("kitName", kit))).findIds();
			
			database.delete(Cooldown.class, ids);
		} catch(PersistenceException e) {
			logger.log(Level.WARNING, "Could not delete cooldown in database!", e);
		}
	}
}