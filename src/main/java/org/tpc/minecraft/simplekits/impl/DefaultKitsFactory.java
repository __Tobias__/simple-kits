package org.tpc.minecraft.simplekits.impl;

import static org.tpc.minecraft.simplekits.Constants.DefaultResources.*;
import static org.tpc.minecraft.simplekits.Constants.GlobalMessages.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.StringJoiner;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.potion.PotionEffectType;
import org.tpc.minecraft.simplekits.Configuration;
import org.tpc.minecraft.simplekits.GlobalMessages;
import org.tpc.minecraft.simplekits.IAliasMap;
import org.tpc.minecraft.simplekits.IKits;
import org.tpc.minecraft.simplekits.IKitsFactory;
import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.conf.adapter.BaseTypeAdapterFactory;
import org.tpc.minecraft.simplekits.conf.adapter.ComponentTypeAdapter;
import org.tpc.minecraft.simplekits.conf.adapter.EnchantmentTypeAdapter;
import org.tpc.minecraft.simplekits.conf.adapter.ItemTypeAdapterFactory;
import org.tpc.minecraft.simplekits.conf.adapter.KitTypeAdapter;
import org.tpc.minecraft.simplekits.conf.adapter.bukkit.BukkitEnchantmentTypeAdapter;
import org.tpc.minecraft.simplekits.conf.adapter.bukkit.BukkitPotionEffectTypeAdapter;
import org.tpc.minecraft.simplekits.conf.impl.BaseReferenceConf;
import org.tpc.minecraft.simplekits.conf.impl.ComponentConf;
import org.tpc.minecraft.simplekits.conf.impl.EnchantmentConf;
import org.tpc.minecraft.simplekits.conf.impl.ItemConf;
import org.tpc.minecraft.simplekits.conf.impl.KitConf;
import org.tpc.minecraft.simplekits.conf.impl.ReferenceConf;
import org.tpc.minecraft.simplekits.def.IAmountFactory;
import org.tpc.minecraft.simplekits.def.IEnchantmentFactory;
import org.tpc.minecraft.simplekits.def.IMaterialFactory;
import org.tpc.minecraft.simplekits.def.ISlotFactory;
import org.tpc.minecraft.simplekits.def.impl.amounts.DefaultAmountFactory;
import org.tpc.minecraft.simplekits.def.impl.enchantments.DefaultEnchantmentFactory;
import org.tpc.minecraft.simplekits.def.impl.materials.DefaultMaterialFactory;
import org.tpc.minecraft.simplekits.def.impl.slots.DefaultSlotFactory;
import org.tpc.minecraft.simplekits.exceptions.KitCircularDependencyException;
import org.tpc.minecraft.simplekits.exceptions.KitDependencyException;
import org.tpc.minecraft.simplekits.exceptions.KitDependencyNotFoundException;
import org.tpc.minecraft.simplekits.kit.IBaseReference;
import org.tpc.minecraft.simplekits.kit.IComponent;
import org.tpc.minecraft.simplekits.kit.IItem;
import org.tpc.minecraft.simplekits.kit.IKit;
import org.tpc.minecraft.simplekits.kit.IReference;
import org.tpc.minecraft.simplekits.kit.impl.BaseReference;
import org.tpc.minecraft.simplekits.kit.impl.Enchantment;
import org.tpc.minecraft.simplekits.kit.impl.Item;
import org.tpc.minecraft.simplekits.kit.impl.Kit;
import org.tpc.minecraft.simplekits.kit.impl.Reference;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class DefaultKitsFactory implements IKitsFactory {
	
	protected static class Cache<CacheType> {
		
		private Map<String, CacheType> objects;
		private AliasMap aliasMap;
		
		protected Cache() {
			objects = new TreeMap<>();
			aliasMap = new AliasMap();
		}
		
		protected Cache(AliasMap aliasMap) {
			this.aliasMap = aliasMap;
		}
		
		public Map<String, CacheType> getObjects() {
			return objects;
		}
		
		public AliasMap getAliases() {
			return aliasMap;
		}
		
		public boolean exists(String name) {
			return objects.containsKey(name);
		}
	}
	
	private Gson gson;
	
	private IAmountFactory amountFactory;
	private IEnchantmentFactory enchantmentFactory;
	private IMaterialFactory materialFactory;
	private ISlotFactory slotFactory;
	
	private Configuration configuration;
	private Logger logger;
	
	public DefaultKitsFactory(SimpleKits plugin) {
		this.configuration = plugin.getConfiguration();
		this.logger = plugin.getLogger();
		
		amountFactory = new DefaultAmountFactory(plugin);
		enchantmentFactory = new DefaultEnchantmentFactory(plugin);
		materialFactory = new DefaultMaterialFactory(plugin);
		slotFactory = new DefaultSlotFactory(plugin);
		
		KitTypeAdapter kitTypeAdapter = new KitTypeAdapter();
		ComponentTypeAdapter componentTypeAdapter = new ComponentTypeAdapter();
		EnchantmentTypeAdapter enchantmentTypeAdapter = new EnchantmentTypeAdapter(enchantmentFactory);
		BukkitEnchantmentTypeAdapter bukkitEnchantmentTypeAdapter = new BukkitEnchantmentTypeAdapter();
		BukkitPotionEffectTypeAdapter bukkitPotionEffectTypeAdapter = new BukkitPotionEffectTypeAdapter();
		
		BaseTypeAdapterFactory baseTypeAdapterFactory = new BaseTypeAdapterFactory();
		ItemTypeAdapterFactory itemTypeAdapterFactory = new ItemTypeAdapterFactory(slotFactory, materialFactory, amountFactory);
		
		gson = new GsonBuilder()
				.excludeFieldsWithoutExposeAnnotation()
				.setPrettyPrinting()
				
				.registerTypeAdapter(KitConf.class, kitTypeAdapter)
				
				.registerTypeAdapter(ComponentConf.class, componentTypeAdapter)
				.registerTypeAdapter(EnchantmentConf.class, enchantmentTypeAdapter)
				
				.registerTypeAdapter(org.bukkit.enchantments.Enchantment.class, bukkitEnchantmentTypeAdapter)
				.registerTypeAdapter(PotionEffectType.class, bukkitPotionEffectTypeAdapter)
				
				.registerTypeAdapterFactory(baseTypeAdapterFactory)
				.registerTypeAdapterFactory(itemTypeAdapterFactory)
				.create();
		
		kitTypeAdapter.setGson(gson);
		componentTypeAdapter.setGson(gson);
		baseTypeAdapterFactory.setGson(gson);
	}
	
	@Override
	public IKits load() {
		// Initialize Factories
		amountFactory.load(configuration.getBasePath().resolve(AMOUNTS_FILE_NAME));
		enchantmentFactory.load(configuration.getBasePath().resolve(ENCHANTMENTS_FILE_NAME));
		materialFactory.load(configuration.getBasePath().resolve(MATERIALS_FILE_NAME));
		slotFactory.load(configuration.getBasePath().resolve(SLOTS_FILE_NAME));
		
		Cache<IKit> templates = loadTemplates();
		IAliasMap templateAliasMap = templates.getAliases();
		
		Cache<IKit> kits = loadKits(templates);
		IAliasMap kitAliasMap = kits.getAliases();
		
		Set<String> loadedTemplates = templates.getObjects().keySet();
		Set<String> loadedKits = kits.getObjects().keySet();
		
		logger.log(Level.INFO, GlobalMessages.getString(GMESSAGE_CONFIG_INFO_TEMPLATES_LOADED, loadedTemplates.size()));
		for(String templateName : loadedTemplates) {
			StringJoiner stringJoiner = new StringJoiner(", ");
			
			for(String alias : templateAliasMap.getAliases(templateName)) {
				stringJoiner.add(alias);
			}
			
			logger.log(Level.INFO, GlobalMessages.getString(GMESSAGE_CONFIG_INFO_TEMPLATE_LOADED, templateName, stringJoiner.toString()));
		}
		
		logger.log(Level.INFO, GlobalMessages.getString(GMESSAGE_CONFIG_INFO_KITS_LOADED, loadedKits.size()));
		for(String kitName : loadedKits) {
			StringJoiner stringJoiner = new StringJoiner(", ");
			
			for(String alias : kitAliasMap.getAliases(kitName)) {
				stringJoiner.add(alias);
			}
			
			logger.log(Level.INFO, GlobalMessages.getString(GMESSAGE_CONFIG_INFO_KIT_LOADED, kitName, stringJoiner.toString()));
		}
		
		return new Kits(kits.getObjects(), kits.getAliases());
	}
	
	private Cache<IKit> loadTemplates() {
		Cache<KitConf> cache = new Cache<>();
		
		for(Path path : configuration.getTemplatePaths()) {
			try(DirectoryStream<Path> stream = Files.newDirectoryStream(path, configuration.getTemplateFilter())) {
				for(Path templatePath : stream) {
					loadTemplates(cache, templatePath);
				}
			} catch (IOException ioException) {
				logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_TEMPLATE_FOLDER_ACCESS,
						path), ioException);
			}
		}
		
		return initializeTemplates(cache);
	}
	
	private void loadTemplates(Cache<KitConf> cache, Path path) {
		Map<String, KitConf> templates = cache.getObjects();
		AliasMap aliasMap = cache.getAliases();
		
		try {
			@SuppressWarnings("serial")
			Map<String, KitConf> readRemplates = gson.fromJson(new String(Files.readAllBytes(path), StandardCharsets.UTF_8),
					new TypeToken<Map<String, KitConf>>() {}.getType());
			
			for(Entry<String, KitConf> entry : readRemplates.entrySet()) {
				String name = entry.getKey();
				KitConf configuration = entry.getValue();
				configuration.setSource(path);
				
				if(templates.containsKey(name)) {
					logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_TEMPLATE_TEMPLATE_TEMPLATE_EXISTS,
							name, path));
					continue;
				}
				
				if(aliasMap.aliasExists(name)) {
					logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_TEMPLATE_TEMPLATE_ALIAS_EXISTS,
							name, path, aliasMap.getObjectForAlias(name)));
					aliasMap.removeAlias(name);
				}
				
				templates.put(name, configuration);
				
				for(String alias : configuration.getAliases()) {
					if(templates.containsKey(alias)) {
						logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_TEMPLATE_ALIAS_TEMPLATE_EXISTS,
								alias, path, name));
						continue;
					}
					
					if(aliasMap.aliasExists(alias)) {
						logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_TEMPLATE_ALIAS_ALIAS_EXISTS,
								alias, path, aliasMap.getObjectForAlias(alias), name));
						aliasMap.removeAlias(alias);
					}
					
					aliasMap.addAlias(name, alias);
				}
			}
		} catch(IOException | JsonSyntaxException exception) {
			if(exception instanceof JsonSyntaxException) {
				logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_TEMPLATE_LOAD, path, exception.getLocalizedMessage()));
				return;
			}
			
			logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_TEMPLATE_ACCESS, path), exception);
		}
	}
	
	private Cache<IKit> initializeTemplates(Cache<KitConf> cache) {
		AliasMap aliases = cache.getAliases();
		Cache<IKit> templateCache = new Cache<>();
		Map<String, IKit> loadedTemplates = templateCache.getObjects();
		
		for(Entry<String, KitConf> template : cache.getObjects().entrySet()) {
			String name = template.getKey();
			KitConf configuration = template.getValue();
			
			Kit kit = new Kit(configuration.getCooldown(), name);
			kit.setSource(configuration.getSource());
			
			// Add all components
			for(ComponentConf component : configuration.getItems()) {
				if(component instanceof ReferenceConf) {
					ReferenceConf reference = ((ReferenceConf)component);
					switch(reference.getType()) {
					case Kit:
						logger.log(Level.WARNING, GlobalMessages.getString(
								GMESSAGE_CONFIG_ERROR_KIT_REFERENCE_IN_TEMPLATE_REF, name));
						continue;
					case Reference:
					case Template:
					default:
						kit.addComponent(Reference.template(reference.getName()));
						break;
					}
				} else if(component instanceof ItemConf){
					kit.addComponent(copyItem((ItemConf) component));
				}
			}
			
			// Add base references (sorted by priority)
			List<BaseReferenceConf> baseReferences = new ArrayList<>(configuration.getBases());
			Collections.sort(baseReferences, Comparator.comparing(BaseReferenceConf::getPriority));
			for(BaseReferenceConf reference : baseReferences) {
				switch(reference.getType()) {
				case Kit:
					logger.log(Level.WARNING, GlobalMessages.getString(
							GMESSAGE_CONFIG_ERROR_KIT_REFERENCE_IN_TEMPLATE_BASE, name));
					continue;
				case Reference:
				case Template:
				default:
					kit.addBase(BaseReference.template(reference.getName()));
					break;
				}
			}
			
			loadedTemplates.put(name, kit);
		}
		
		// Sort faulty templates
		boolean checked;
		Set<String> checkedTemplates;
		do {
			checked = true;
			checkedTemplates = new HashSet<>();
			
			// Check each template
			for(Entry<String, IKit> kitEntry : loadedTemplates.entrySet()) {
				try {
					checkTemplateDependencies(templateCache, kitEntry.getValue(), new Stack<String>(), checkedTemplates);
				} catch(KitDependencyException dependencyException) {
					if(dependencyException instanceof KitDependencyNotFoundException) {
						logger.log(Level.WARNING, GlobalMessages.getString(dependencyException.isBaseReference()
								? GMESSAGE_CONFIG_ERROR_TEMPLATE_REFERENCE_NOT_FOUND_BASE
										: GMESSAGE_CONFIG_ERROR_TEMPLATE_REFERENCE_NOT_FOUND_REF,
								dependencyException.getKitName(), dependencyException.getDependencyName()));
					} else if(dependencyException instanceof KitCircularDependencyException) {
						logger.log(Level.WARNING, GlobalMessages.getString(dependencyException.isBaseReference()
								? GMESSAGE_CONFIG_ERROR_TEMPLATE_REFERENCE_CIRCULAR_BASE
										: GMESSAGE_CONFIG_ERROR_TEMPLATE_REFERENCE_CIRCULAR_REF,
								dependencyException.getKitName(), dependencyException.getDependencyName()));
					}
					
					String name = dependencyException.getKitName();
					loadedTemplates.remove(name);
					aliases.removeObject(name);
					
					checked = false;
					break;
				}
			}
		} while(!checked);
		
		// Fill references
		for(Entry<String, IKit> template : loadedTemplates.entrySet()) {
			IKit kit = template.getValue();
			
			for(IBaseReference reference : kit.getBases()) {
				BaseReference baseReference = (BaseReference) reference;
				String name = baseReference.getName();
				
				baseReference.setKit(loadedTemplates.get(aliases.aliasExists(name)
						? aliases.getObjectForAlias(name) : name));
			}
			
			for(IComponent component : kit.getComponents()) {
				if(!(component instanceof IReference)) {
					continue;
				}
				
				Reference reference = (Reference) component;
				String name = reference.getName();
				
				reference.setKit(loadedTemplates.get(aliases.aliasExists(name)
						? aliases.getObjectForAlias(name) : name));
			}
		}
		
		return templateCache;
	}
	
	private void checkTemplateDependencies(Cache<IKit> cache, IKit kit, Stack<String> names, Set<String> checkedTemplates) throws KitDependencyException {
		Map<String, IKit> templates = cache.getObjects();
		AliasMap aliasMap = cache.getAliases();
		
		names.push(kit.getName());
		
		for(IComponent component : kit.getComponents()) {
			if(!(component instanceof IReference)) {
				continue;
			}
			
			IReference reference = (IReference) component;
			String refName = reference.getName();
			String refAlias = null;
			
			if(aliasMap.aliasExists(refName)) {
				refName = aliasMap.getObjectForAlias(refAlias = refName);
			}
			
			if(checkedTemplates.contains(refName)) {
				continue;
			}
			
			if(!templates.containsKey(refName)) {
				throw new KitDependencyNotFoundException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
			}
			
			if(names.contains(refName)) {
				throw new KitCircularDependencyException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
			}
			
			checkTemplateDependencies(cache, templates.get(refName), names, checkedTemplates);
		}
		
		for(IBaseReference reference : kit.getBases()) {
			String refName = reference.getName();
			String refAlias = null;
			
			if(aliasMap.aliasExists(refName)) {
				refName = aliasMap.getObjectForAlias(refAlias = refName);
			}
			
			if(checkedTemplates.contains(refName)) {
				continue;
			}
			
			if(!templates.containsKey(refName)) {
				throw new KitDependencyNotFoundException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
			}
			
			if(names.contains(refName)) {
				throw new KitCircularDependencyException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
			}
			
			checkTemplateDependencies(cache, templates.get(refName), names, checkedTemplates);
		}
		
		checkedTemplates.add(kit.getName());
	}
	
	private void checkKitDependencies(Cache<IKit> cache, Cache<IKit> templates, IKit kit, Stack<String> names, Set<String> checkedKits) throws KitDependencyException {
		Map<String, IKit> kits = cache.getObjects();
		AliasMap aliasMap = cache.getAliases();
		
		Map<String, IKit> templateObjects = templates.getObjects();
		AliasMap templateAliases = templates.getAliases();
		
		names.push(kit.getName());
		
		for(IComponent component : kit.getComponents()) {
			if(!(component instanceof IReference)) {
				continue;
			}
			
			Reference reference = (Reference) component;
			String refName = reference.getName();
			String refAlias = null;
			
			switch(reference.getType()) {
			case Kit:
				if(aliasMap.aliasExists(refName)) {
					refName = aliasMap.getObjectForAlias(refAlias = refName);
				}
				
				if(checkedKits.contains(refName)) {
					continue;
				}
				
				if(!kits.containsKey(refName)) {
					throw new KitDependencyNotFoundException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
				}
				
				if(names.contains(refName)) {
					throw new KitCircularDependencyException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
				}
				
				checkKitDependencies(cache, templates, kits.get(refName), names, checkedKits);
				break;
			case Reference:
				if(aliasMap.aliasExists(refName)) {
					refName = aliasMap.getObjectForAlias(refAlias = refName);
				}
				
				if(checkedKits.contains(refName)) {
					continue;
				}
				
				if(!kits.containsKey(refName)) {
					throw new KitDependencyNotFoundException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
				}
				
				if(names.contains(refName)) {
					throw new KitCircularDependencyException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
				}
				
				checkKitDependencies(cache, templates, kits.get(refName), names, checkedKits);
				// Drop trough
			case Template:
				if(templateAliases.aliasExists(refName)) {
					refName = templateAliases.getObjectForAlias(refAlias = refName);
				}
				
				if(!templateObjects.containsKey(refName)) {
					throw new KitDependencyNotFoundException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
				}
				break;
			default:
				assert false : "This point should never be reached"; //$NON-NLS-1$
				break;
			}
		}
		
		for(IBaseReference iReference : kit.getBases()) {
			BaseReference reference = (BaseReference) iReference;
			String refName = reference.getName();
			String refAlias = null;
			
			switch(reference.getType()) {
			case Kit:
				if(aliasMap.aliasExists(refName)) {
					refName = aliasMap.getObjectForAlias(refAlias = refName);
				}
				
				if(checkedKits.contains(refName)) {
					continue;
				}
				
				if(!kits.containsKey(refName)) {
					throw new KitDependencyNotFoundException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
				}
				
				if(names.contains(refName)) {
					throw new KitCircularDependencyException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
				}
				
				checkKitDependencies(cache, templates, kits.get(refName), names, checkedKits);
				break;
			case Reference:
				if(aliasMap.aliasExists(refName)) {
					refName = aliasMap.getObjectForAlias(refAlias = refName);
				}
				
				if(checkedKits.contains(refName)) {
					continue;
				}
				
				if(!kits.containsKey(refName)) {
					throw new KitDependencyNotFoundException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
				}
				
				if(names.contains(refName)) {
					throw new KitCircularDependencyException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
				}
				
				checkKitDependencies(cache, templates, kits.get(refName), names, checkedKits);
				// Drop trough
			case Template:
				if(templateAliases.aliasExists(refName)) {
					refName = templateAliases.getObjectForAlias(refAlias = refName);
				}
				
				if(!templateObjects.containsKey(refName)) {
					throw new KitDependencyNotFoundException(names.firstElement(), (refAlias != null) ? refAlias : refName, false);
				}
				break;
			default:
				assert false : "This point should never be reached"; //$NON-NLS-1$
				break;
			}
		}
		
		checkedKits.add(kit.getName());
	}
	
	private Cache<IKit> loadKits(Cache<IKit> templates) {
		Cache<KitConf> cache = new Cache<>();
		
		for(Path path : configuration.getKitPaths()) {
			try(DirectoryStream<Path> stream = Files.newDirectoryStream(path, configuration.getKitFilter())) {
				for(Path kitPath : stream) {
					loadKits(cache, kitPath, templates);
				}
			} catch (IOException ioException) {
				logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_KIT_FOLDER_ACCESS,
						path), ioException);
			}
		}
		
		return initializeKits(cache, templates);
	}
	
	private void loadKits(Cache<KitConf> cache, Path path, Cache<IKit> templates) {
		AliasMap templateAliases = templates.getAliases();
		Map<String, IKit> templateObjects = templates.getObjects();
		
		Map<String, KitConf> kits = cache.getObjects();
		AliasMap aliases = cache.getAliases();
		
		try {
			@SuppressWarnings("serial")
			Map<String, KitConf> readKits = gson.fromJson(new String(Files.readAllBytes(path), StandardCharsets.UTF_8),
					new TypeToken<Map<String, KitConf>>() {}.getType());
			
			for(Entry<String, KitConf> entry : readKits.entrySet()) {
				String name = entry.getKey();
				KitConf configuration = entry.getValue();
				configuration.setSource(path);
				
				if(templateObjects.containsKey(name)) {
					logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_KIT_KIT_TEMPLATE_EXISTS, name, path));
					continue;
				}
				
				if(templateAliases.aliasExists(name)) {
					logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_KIT_KIT_ALIAS_TEMPLATE_EXISTS,
							name, path, templateAliases.getObjectForAlias(name)));
				}
				
				if(kits.containsKey(name)) {
					logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_KIT_KIT_KIT_EXISTS, name, path));
					continue;
				}
				
				if(aliases.aliasExists(name)) {
					logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_KIT_KIT_ALIAS_KIT_EXISTS,
							name, path, aliases.getObjectForAlias(name)));
					aliases.removeAlias(name);
				}
				
				kits.put(name, configuration);
				
				for(String alias : configuration.getAliases()) {
					if(templateObjects.containsKey(alias)) {
						logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_KIT_ALIAS_TEMPLATE_EXISTS,
								alias, path, name));
						continue;
					}
					
					if(templateAliases.aliasExists(alias)) {
						logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_KIT_ALIAS_ALIAS_TEMPLATE_EXISTS,
								alias, path, templateAliases.getObjectForAlias(alias), name));
					}
					
					if(kits.containsKey(alias)) {
						logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_KIT_ALIAS_KIT_EXISTS,
								alias, path, name));
						continue;
					}
					
					if(aliases.aliasExists(alias)) {
						logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_KIT_ALIAS_ALIAS_KIT_EXISTS,
								alias, path, aliases.getObjectForAlias(alias), name));
						aliases.removeAlias(name);
					}
					
					aliases.addAlias(name, alias);
				}
			}
		} catch(IOException | JsonSyntaxException exception) {
			if(exception instanceof JsonSyntaxException) {
				logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_KIT_LOAD, path, exception.getLocalizedMessage()));
				return;
			}
			
			logger.log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_KIT_ACCESS, path), exception);
		}
	}
	
	private Cache<IKit> initializeKits(Cache<KitConf> cache, Cache<IKit> templates) {
		AliasMap aliases = cache.getAliases();
		Cache<IKit> kitCache = new Cache<>();
		Map<String, IKit> loadedKits = kitCache.getObjects();
		
		AliasMap templateAliases = templates.getAliases();
		Map<String, IKit> templateObjects = templates.getObjects();
		
		for(Entry<String, KitConf> template : cache.getObjects().entrySet()) {
			String name = template.getKey();
			KitConf configuration = template.getValue();
			
			Kit kit = new Kit(configuration.getCooldown(), name);
			kit.setSource(configuration.getSource());
			
			// Add all components
			for(ComponentConf component : configuration.getItems()) {
				if(component instanceof ReferenceConf) {
					ReferenceConf reference = ((ReferenceConf)component);
					switch(reference.getType()) {
					case Kit:
						kit.addComponent(Reference.kit(reference.getName()));
						break;
					case Reference:
						kit.addComponent(Reference.reference(reference.getName()));
						break;
					case Template:
						kit.addComponent(Reference.template(reference.getName()));
						break;
					default:
						assert false : "This point should never be reached"; //$NON-NLS-1$
						break;
					}
				} else if(component instanceof ItemConf){
					kit.addComponent(copyItem((ItemConf) component));
				}
			}
			
			// Add base references (sorted by priority)
			List<BaseReferenceConf> baseReferences = new ArrayList<>(configuration.getBases());
			Collections.sort(baseReferences, Comparator.comparing(BaseReferenceConf::getPriority));
			for(BaseReferenceConf reference : baseReferences) {
				switch(reference.getType()) {
				case Kit:
					kit.addBase(BaseReference.kit(reference.getName()));
					break;
				case Reference:
					kit.addBase(BaseReference.reference(reference.getName()));
					break;
				case Template:
					kit.addBase(BaseReference.template(reference.getName()));
					break;
				default:
					assert false : "This point should never be reached"; //$NON-NLS-1$
					break;
				}
			}
			
			loadedKits.put(name, kit);
		}
		
		// Sort faulty kits
		boolean checked;
		Set<String> checkedKits;
		do {
			checked = true;
			checkedKits = new HashSet<>();
			
			// Check each kit
			for(Entry<String, IKit> kit : loadedKits.entrySet()) {
				try {
					checkKitDependencies(kitCache, templates, kit.getValue(), new Stack<String>(), checkedKits);
				} catch(KitDependencyException dependencyException) {
					if(dependencyException instanceof KitDependencyNotFoundException) {
						logger.log(Level.WARNING, GlobalMessages.getString(dependencyException.isBaseReference()
								? GMESSAGE_CONFIG_ERROR_KIT_REFERENCE_NOT_FOUND_BASE
										: GMESSAGE_CONFIG_ERROR_KIT_REFERENCE_NOT_FOUND_REF,
								dependencyException.getKitName(), dependencyException.getDependencyName()));
					} else if(dependencyException instanceof KitCircularDependencyException) {
						logger.log(Level.WARNING, GlobalMessages.getString(dependencyException.isBaseReference()
								? GMESSAGE_CONFIG_ERROR_KIT_REFERENCE_CIRCULAR_BASE
										: GMESSAGE_CONFIG_ERROR_KIT_REFERENCE_CIRCULAR_REF,
								dependencyException.getKitName(), dependencyException.getDependencyName()));
					}
					
					String name = dependencyException.getKitName();
					loadedKits.remove(name);
					aliases.removeObject(name);
					
					checked = false;
					break;
				}
			}
		} while(!checked);
		
		// Fill references
		for(Entry<String, IKit> kitEntry : loadedKits.entrySet()) {
			IKit kit = kitEntry.getValue();
			
			for(IBaseReference reference : kit.getBases()) {
				BaseReference baseReference = (BaseReference) reference;
				String name = baseReference.getName();
				
				switch(baseReference.getType()) {
				case Kit:
					baseReference.setKit(loadedKits.get(aliases.aliasExists(name)
							? aliases.getObjectForAlias(name) : name));
					break;
				case Reference:
					if(aliases.aliasExists(name) || loadedKits.containsKey(name)) {
						baseReference.setKit(loadedKits.get(aliases.aliasExists(name)
								? aliases.getObjectForAlias(name) : name));
						break;
					}
					// Drop trough
				case Template:
					baseReference.setKit(templateObjects.get(templateAliases.aliasExists(name)
						? templateAliases.getObjectForAlias(name) : name));
					break;
				default:
					assert false : "This point should never be reached"; //$NON-NLS-1$
					break;
					
				}
			}
			
			for(IComponent component : kit.getComponents()) {
				if(!(component instanceof IReference)) {
					continue;
				}
				
				Reference reference = (Reference) component;
				String name = reference.getName();
				
				switch(reference.getType()) {
				case Kit:
					reference.setKit(loadedKits.get(aliases.aliasExists(name)
							? aliases.getObjectForAlias(name) : name));
					break;
				case Reference:
					if(aliases.aliasExists(name) || loadedKits.containsKey(name)) {
						reference.setKit(loadedKits.get(aliases.aliasExists(name)
								? aliases.getObjectForAlias(name) : name));
						break;
					}
					// Drop trough
				case Template:
					reference.setKit(templateObjects.get(templateAliases.aliasExists(name)
						? templateAliases.getObjectForAlias(name) : name));
					break;
				default:
					assert false : "This point should never be reached"; //$NON-NLS-1$
					break;
				}
			}
		}
		
		return kitCache;
	}
	
	private IItem copyItem(ItemConf itemConf) {
		Item item = new Item(amountFactory, itemConf.getId(), itemConf.getAmount(), itemConf.getSlot());
		
		item.setDamage(itemConf.getDamage());
		
		if(itemConf.isPotion()) {
			item.setPotion(true);
			item.setPotionEffects(itemConf.getEffects());
		}
		
		if(itemConf.isEnchanted()) {
			item.setEnchanted(true);
			item.setItemName(itemConf.getItemName());
			
			for(EnchantmentConf enchantmentConf : itemConf.getEnchantments()) {
				Enchantment enchantment = new Enchantment(enchantmentConf.getType());
				
				enchantment.setLevel((int) enchantmentConf.getLevel());
				enchantment.setName(enchantmentConf.getName());
				
				item.addEnchantment(enchantment);
			}
		}
		
		return item;
	}
}