package org.tpc.minecraft.simplekits.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.TreeMap;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemFactory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;
import org.tpc.minecraft.simplekits.IKitPlacer;
import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.impl.KitPlacer.ItemPlaceData.PlaceItem;
import org.tpc.minecraft.simplekits.impl.KitPlacer.ItemPlaceData.SlotItem;
import org.tpc.minecraft.simplekits.kit.IAbort;
import org.tpc.minecraft.simplekits.kit.IBaseReference;
import org.tpc.minecraft.simplekits.kit.IComponent;
import org.tpc.minecraft.simplekits.kit.IDiscard;
import org.tpc.minecraft.simplekits.kit.IEnchantableItem;
import org.tpc.minecraft.simplekits.kit.IEnchantment;
import org.tpc.minecraft.simplekits.kit.IItem;
import org.tpc.minecraft.simplekits.kit.IKit;
import org.tpc.minecraft.simplekits.kit.IMaterial;
import org.tpc.minecraft.simplekits.kit.IOverwrite;
import org.tpc.minecraft.simplekits.kit.IPotion;
import org.tpc.minecraft.simplekits.kit.IPotionEffect;
import org.tpc.minecraft.simplekits.kit.IReference;
import org.tpc.minecraft.simplekits.kit.ISlot;

public class KitPlacer implements IKitPlacer {
	
	private static final int getDamage(int damage) {
		return Math.max(damage, 0);
	}
	
	protected static class ItemPlaceData {
		
		protected static class SlotItem {
			
			private IOverwrite overwrite;
			private IItem item;
			
			protected SlotItem(IOverwrite overwrite, IItem item) {
				this.item = item;
				this.overwrite = overwrite;
			}
			
			public IItem getItem() {
				return item;
			}
			
			public IOverwrite getOverwrite() {
				return overwrite;
			}
		}
		
		protected static class PlaceItem {
			
			private IDiscard discard;
			private IItem item;
			
			protected PlaceItem(IDiscard discard, IItem item) {
				this.item = item;
				this.discard = discard;
			}
			
			public IItem getItem() {
				return item;
			}
			
			public IDiscard getDiscard() {
				return discard;
			}
		}
		
		private List<PlaceItem> items;
		private Map<Long, SlotItem> itemsInSlot;
		
		protected ItemPlaceData() {
			items = new ArrayList<>();
			itemsInSlot = new TreeMap<>();
		}
		
		public void addItem(PlaceItem item) {
			items.add(item);
		}
		
		public void removeItem(PlaceItem item) {
			items.remove(item);
		}
		
		public void addSlotItem(Long slot, SlotItem item) {
			itemsInSlot.put(slot, item);
		}
		
		public void removeSlotItem(Long slot) {
			itemsInSlot.remove(slot);
		}
		
		public SlotItem getSlotItem(Long slot) {
			return itemsInSlot.get(slot);
		}
		
		public List<PlaceItem> getItems() {
			return Collections.unmodifiableList(items);
		}
		
		public Map<Long, SlotItem> getItemsInSlot() {
			return Collections.unmodifiableMap(itemsInSlot);
		}
	}
	
	private Server server;
	
	public KitPlacer(SimpleKits plugin) {
		this.server = plugin.getServer();
	}
	
	private ItemPlaceData merge(IKit kit, IOverwrite overwrite, IDiscard discard, IAbort abort) {
		ItemPlaceData placeData = new ItemPlaceData();
		if(!merge(kit, placeData, overwrite, discard, false)) {
			return null;
		}
		
		return placeData;
	}
	
	private boolean merge(IKit kit, ItemPlaceData placeData, IOverwrite overwrite, IDiscard discard, boolean ignoreSlots) {
		for(IBaseReference baseReference : kit.getBases()) {
			IKit subKit = baseReference.getKit();
			assert subKit != null : "Invalid base-reference found: " + baseReference.getName();
			
			if(!merge(subKit, placeData, overwrite.merge(subKit), discard.merge(subKit), ignoreSlots)) {
				return false;
			}
		}
		
		for(IComponent item : kit.getComponents()) {
			if(item instanceof IReference) {
				IReference reference = (IReference) item;
				
				IKit subKit = reference.getKit();
				assert subKit != null : "Invalid reference found: " + reference.getName();
				
				if(!merge(reference.getKit(), placeData, overwrite.merge(subKit), discard.merge(subKit), (ignoreSlots || reference.isIgnoringSlots()))) {
					return false;
				}
				continue;
			}
			
			if(item instanceof IItem) {
				IItem normalItem = (IItem) item;
				ISlot slot = normalItem.getSlot();
				if(!ignoreSlots && (slot != null)) {
					if(!mergeSlot(slot, normalItem, overwrite, discard, placeData)) {
						return false;
					}
				} else {
					placeData.addItem(new PlaceItem(discard, normalItem));
				}
				continue;
			}
		}
		
		return true;
	}
	
	private boolean mergeSlot(ISlot slot, IItem item, IOverwrite overwrite, IDiscard discard, ItemPlaceData placeData) {
		Long slotValue = Long.valueOf(slot.getSlot());
		IMaterial material = item.getMaterial();
		
		SlotItem itemInSlot = placeData.getSlotItem(slotValue);
		if((itemInSlot == null) || overwrite.isOverwrite()) {
			long amount = item.getAmount().getValue(material);
			long setAmount = Math.min(material.getMaterial().getMaxStackSize(), amount);
			long remaining = amount - setAmount;
			
			placeData.addSlotItem(slotValue, new SlotItem(overwrite, item.createStack(setAmount)));
			if(remaining > 0) {
				placeData.addItem(new PlaceItem(discard, item.createStack(remaining)));
			}
			return true;
		}
		
		if(!isMergeable(itemInSlot.getItem(), item)) {
			if(!discard.isDiscardIfOccupied()) {
				placeData.addItem(new PlaceItem(discard, item));
			}
			return true;
		}
		
		IItem iitemInSlot = itemInSlot.getItem();
		IMaterial materialInSlot = iitemInSlot.getMaterial();
		long amount = iitemInSlot.getAmount().getValue(materialInSlot) + item.getAmount().getValue(material);
		IItem megredItem = item.createStack(Math.min(amount, item.getMaterial().getMaterial().getMaxStackSize()));
		amount -= megredItem.getAmount().getValue(material);
		
		placeData.addSlotItem(slotValue, new SlotItem(overwrite, megredItem));
		
		if(amount > 0 && !discard.isDiscardIfOccupied()) {
			placeData.addItem(new PlaceItem(discard, item.createStack(amount)));
		}
		
		return true;
	}
	
	private boolean isMergeable(IItem firstItem, IItem secondItem) {
		if(firstItem.getClass() != secondItem.getClass()) {
			return false;
		}
		
		if(!Objects.equals(firstItem.getMaterial().getMaterial(), secondItem.getMaterial().getMaterial())
				|| !Objects.equals(getDamage(firstItem.getDamage()), getDamage(secondItem.getDamage()))) {
			return false;
		}
		
		if(firstItem instanceof IEnchantableItem) {
			return Objects.equals(((IEnchantableItem)firstItem).getItemName(), ((IEnchantableItem)secondItem).getItemName())
					&& Objects.equals(((IEnchantableItem)firstItem).getEnchantments(), ((IEnchantableItem)secondItem).getEnchantments());
		}
		
		if(firstItem instanceof IPotion) {
			return Objects.equals(((IPotion)firstItem).getEffects(), ((IPotion)secondItem).getEffects());
		}
		
		return true;
	}
	
	public boolean place(PlayerInventory playerInventory, IKit kit, IOverwrite overwrite, IDiscard discard, IAbort abort) {
		Inventory mainInventory = copyInventory(playerInventory.getContents(), playerInventory.getHolder());
		ItemStack[] armorInventory = copyInventory(playerInventory.getArmorContents());
		
		ItemFactory itemFactory = server.getItemFactory();
		ItemPlaceData itemsToPlace = merge(kit, overwrite, discard, abort);
		
		for(Entry<Long, SlotItem> itemEntry : itemsToPlace.getItemsInSlot().entrySet()) {
			int slot = (int) itemEntry.getKey().longValue();
			ItemStack stackInSlot = ((slot > 35) ? armorInventory[slot - 100] : mainInventory.getItem(slot));
			IItem item = itemEntry.getValue().getItem();
			
			ItemStack stack = createStack(item, itemFactory);
			long amount = item.getAmount().getValue(item.getMaterial());
			
			if((stackInSlot == null) || (stackInSlot.getType() == Material.AIR)) {
				int maxStackSize = stack.getMaxStackSize();
				if(maxStackSize >= amount) {
					stack.setAmount((int) amount);
				} else {
					stack.setAmount(maxStackSize);
					
					if(!discard.isDiscardIfOccupied()) {
						itemsToPlace.addItem(new PlaceItem(discard, item.createStack(amount - maxStackSize)));
					}
				}
				
				if(slot > 35) {
					armorInventory[slot - 100] = stack;
				} else {
					mainInventory.setItem(slot, stack);
				}
				
				continue;
			}
			
			int maxStackSize = stackInSlot.getMaxStackSize();
			int amountInSlot = stackInSlot.getAmount();
			if(amountInSlot < maxStackSize) {
				if(stack.isSimilar(stackInSlot)) {
					long sum = amountInSlot + amount;
					if(sum >= maxStackSize) {
						stackInSlot.setAmount(maxStackSize);
						
						if(sum > maxStackSize) {
							itemsToPlace.addItem(new PlaceItem(discard, item.createStack(sum - (maxStackSize - amountInSlot))));
						}
						continue;
					}
					
					stackInSlot.setAmount((int) sum);
				} else if(!discard.isDiscardIfOccupied()) {
					itemsToPlace.addItem(new PlaceItem(discard, item));
				}
			} else if(!discard.isDiscardIfOccupied()) {
				itemsToPlace.addItem(new PlaceItem(discard, item));
			}
		}

		List<PlaceItem> placeItems = new ArrayList<>(itemsToPlace.getItems());
		Collection<ItemStack> dropToFloor = new ArrayList<>();
		
		for(int iteration = 0; iteration < 2; iteration++) {
			Collection<ItemStack> items = new ArrayList<>();
			Map<Integer, IDiscard> discards = new HashMap<>();
			
			for(int index = placeItems.size() - 1; index >= 0; index--) {
				PlaceItem itemEntry = placeItems.get(index);
				
				// Only place undiscardable items first 
				if((iteration == 0) && itemEntry.getDiscard().isDiscard()) {
					continue;
				}
				
				placeItems.remove(index);
				
				IItem item = itemEntry.getItem();
				for(long amount = item.getAmount().getValue(item.getMaterial()); amount > 0;) {
					int stackSize = ((amount > Integer.MAX_VALUE) ? Integer.MAX_VALUE : (int) amount);
					
					ItemStack itemStack = createStack(item, itemFactory);
					itemStack.setAmount(stackSize);
					
					discards.put(items.size(), discard.merge(itemEntry.getDiscard()));
					items.add(itemStack);
					
					amount -= stackSize;
				}
			}
			
			Map<Integer, ItemStack> notPlacedItems = mainInventory.addItem(items.toArray(new ItemStack[0]));
			for(Entry<Integer, ItemStack> notPlacedItem : notPlacedItems.entrySet()) {
				IDiscard itemDiscard = discards.get(notPlacedItem.getKey());
				if(itemDiscard.isDiscard()) {
					continue;
				}
				
				if(!itemDiscard.isDropOnFloor()) {
					return false; // Inventory full
				}
				
				dropToFloor.add(notPlacedItem.getValue());
			}
		}
		
		// Save made changes
		playerInventory.setArmorContents(armorInventory);
		playerInventory.setContents(mainInventory.getContents());
		
		Location playerLocation = playerInventory.getHolder().getLocation();
		World world = playerLocation.getWorld();
		for(ItemStack stack : dropToFloor) {
			world.dropItemNaturally(playerLocation, stack);
		}
		
		return true;
	}

	private ItemStack createStack(IItem item, ItemFactory itemFactory) {
		Material material;
		ItemMeta itemMeta = null;
		ItemStack newStack;
		
		if(item.isPotion()) {
			IPotion potion = (IPotion) item;
			newStack = new ItemStack(Material.POTION);
			
			List<IPotionEffect> effects = potion.getEffects();
			if(!effects.isEmpty()) {
				for(IPotionEffect effect : effects) {
					if(itemMeta == null) {
						PotionType potionType = PotionType.getByEffect(effect.getType());
						Potion bukkitPotion = new Potion(potionType, effect.getAmplifier());
						
						bukkitPotion.setSplash(effect.isSplash());
						if(effect.isSplash() && (effect.getDuration() != 0)) {
							bukkitPotion.setHasExtendedDuration(true);
						}
						
						bukkitPotion.apply(newStack);
						
						itemMeta = newStack.getItemMeta();
					} else {
						((PotionMeta) itemMeta).addCustomEffect(
								new PotionEffect(effect.getType(), effect.getDuration(), effect.getAmplifier()), true);
					}
				}
			} else {
				itemMeta = itemFactory.getItemMeta(Material.POTION);
			}
		} else {
			material = item.getMaterial().getMaterial();
			newStack = new ItemStack(material);
			newStack.setDurability((short) getDamage(item.getDamage() < 0 ? item.getMaterial().getDamage() : item.getDamage()));
			itemMeta = newStack.getItemMeta();
		}
		
		if(item.isEnchantable()) {
			IEnchantableItem enchantableItem = (IEnchantableItem) item;
			
			for(IEnchantment enchantment : enchantableItem.getEnchantments()) {
				newStack.addUnsafeEnchantment(enchantment.getEnchantment(), enchantment.getLevel());
			}
			
			itemMeta = newStack.getItemMeta();
			String itemName = enchantableItem.getItemName();
			if(itemName != null) {
				itemMeta.setDisplayName(itemName);
			}
		}
		
		newStack.setItemMeta(itemMeta);
		
		return newStack;
	}
	
	private Inventory copyInventory(ItemStack[] inventoryContents, InventoryHolder holder) {
		Inventory inventory = server.createInventory(holder, inventoryContents.length);
		ItemStack[] clonedInventoryContents = new ItemStack[inventoryContents.length];
		
		for(int index = inventoryContents.length - 1; index >= 0; index--) {
			ItemStack stack = inventoryContents[index];
			if(stack != null) {
				clonedInventoryContents[index] = stack.clone();
			}
		}
		
		inventory.setContents(clonedInventoryContents);
		
		return inventory;
	}
	
	private ItemStack[] copyInventory(ItemStack[] inventoryContents) {
		ItemStack[] clonedInventoryContents = new ItemStack[inventoryContents.length];
		
		for(int index = inventoryContents.length - 1; index >= 0; index--) {
			ItemStack stack = inventoryContents[index];
			if(stack != null) {
				clonedInventoryContents[index] = stack.clone();
			}
		}
		
		return clonedInventoryContents;
	}
}