package org.tpc.minecraft.simplekits;

import java.util.UUID;

import org.tpc.minecraft.simplekits.data.Cooldown;

public interface ICooldowns extends ILoadable {
	
	public void store(String kit, UUID player);
	public Cooldown get(String kit, UUID player);
	public boolean hasCooldown(String kitName, UUID player);
	public void remove(String kit, UUID player);
	
}