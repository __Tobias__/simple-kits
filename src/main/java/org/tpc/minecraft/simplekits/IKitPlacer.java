package org.tpc.minecraft.simplekits;

import org.bukkit.inventory.PlayerInventory;
import org.tpc.minecraft.simplekits.kit.IAbort;
import org.tpc.minecraft.simplekits.kit.IDiscard;
import org.tpc.minecraft.simplekits.kit.IKit;
import org.tpc.minecraft.simplekits.kit.IOverwrite;

public interface IKitPlacer {
	
	public boolean place(PlayerInventory inventory, IKit kit, IOverwrite overwrite, IDiscard discard, IAbort abort);
	
}