package org.tpc.minecraft.simplekits.def.impl;

public class FactoryConstants {
	
	public static final String SLOT_SLOT = "slot";
	public static final String SLOT_ALIASES = "aliases";
	
	public static final String AMOUNT_AMOUNT = "amount";
	public static final String AMOUNT_ALIASES = "aliases";
	
	public static final String MATERIAL_ID = "id";
	public static final String MATERIAL_TYPE = "type";
	public static final String MATERIAL_ALIASES = "aliases";
	public static final String MATERIAL_DAMAGE = "damage";
	
	public static final String ENCHANTMENT_ID = "id";
	public static final String ENCHANTMENT_TYPE = "type";
	public static final String ENCHANTMENT_ALIASES = "aliases";
	
}