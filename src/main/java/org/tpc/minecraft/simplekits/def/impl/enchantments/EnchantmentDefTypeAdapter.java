package org.tpc.minecraft.simplekits.def.impl.enchantments;

import java.io.IOException;

import org.tpc.minecraft.simplekits.def.impl.FactoryConstants;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class EnchantmentDefTypeAdapter extends TypeAdapter<EnchantmentDef> {
	
	@Override
	public void write(JsonWriter out, EnchantmentDef value) throws IOException { }
	
	@Override
	public EnchantmentDef read(JsonReader in) throws IOException {
		JsonElement element = Streams.parse(in);
		if(!element.isJsonObject()) {
			return null;
		}
		
		JsonObject object = element.getAsJsonObject();
		EnchantmentDef enchantment = new EnchantmentDef();
		
		if(object.has(FactoryConstants.ENCHANTMENT_ID)) {
			JsonElement jsonElement = object.get(FactoryConstants.ENCHANTMENT_ID);
			try {
				enchantment.setId(jsonElement.getAsString());
			} catch(ClassCastException | IllegalStateException e) {
				return null;
			}
		} else {
			return null;
		}
		
		if(object.has(FactoryConstants.ENCHANTMENT_ALIASES)) {
			JsonElement jsonElement = object.get(FactoryConstants.MATERIAL_ALIASES);
			try {
				JsonArray jsonArray = jsonElement.getAsJsonArray();
				for(JsonElement subElement : jsonArray) {
					try {
						enchantment.addAlias(subElement.getAsString());
					} catch(ClassCastException | IllegalStateException e) { }
				}
			} catch(ClassCastException | IllegalStateException e) {
				return null;
			}
		}
		
		return enchantment;
	}
}