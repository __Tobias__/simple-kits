package org.tpc.minecraft.simplekits.def.impl.amounts;

import java.io.IOException;

import org.tpc.minecraft.simplekits.def.impl.FactoryConstants;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class AmountDefTypeAdapter extends TypeAdapter<AmountDef> {
	
	@Override
	public void write(JsonWriter out, AmountDef value) throws IOException { }
	
	@Override
	public AmountDef read(JsonReader in) throws IOException {
		JsonElement element = Streams.parse(in);
		if(!element.isJsonObject()) {
			return null;
		}
		JsonObject object = element.getAsJsonObject();
		AmountDef amount = new AmountDef();
		
		if(object.has(FactoryConstants.AMOUNT_AMOUNT)) {
			JsonElement jsonElement = object.get(FactoryConstants.AMOUNT_AMOUNT);
			try {
				amount.setAmount(jsonElement.getAsLong());
			} catch(ClassCastException | IllegalStateException e) {
				return null;
			}
		}
		
		if(object.has(FactoryConstants.AMOUNT_ALIASES)) {
			JsonElement jsonElement = object.get(FactoryConstants.AMOUNT_ALIASES);
			try {
				JsonArray jsonArray = jsonElement.getAsJsonArray();
				for(JsonElement subElement : jsonArray) {
					try {
						amount.addAlias(subElement.getAsString());
					} catch(ClassCastException | IllegalStateException e) { }
				}
			} catch(ClassCastException | IllegalStateException e) {
				return null;
			}
		}
		
		return amount;
	}
}