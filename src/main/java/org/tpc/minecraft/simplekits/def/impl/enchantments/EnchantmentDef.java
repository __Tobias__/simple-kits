package org.tpc.minecraft.simplekits.def.impl.enchantments;

import java.util.ArrayList;
import java.util.List;

public class EnchantmentDef {
	
	private String id;
	private List<String> aliases;
	
	public EnchantmentDef() {
		aliases = new ArrayList<>();
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void addAlias(String alias) {
		aliases.add(alias);
	}
	
	public void removeAlias(String alias) {
		aliases.remove(alias);
	}
	
	public List<String> getAliases() {
		return aliases;
	}
	
	public void setAliases(List<String> aliases) {
		this.aliases = aliases;
	}
}