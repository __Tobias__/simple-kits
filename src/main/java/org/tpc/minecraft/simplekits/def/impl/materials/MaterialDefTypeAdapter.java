package org.tpc.minecraft.simplekits.def.impl.materials;

import java.io.IOException;

import org.tpc.minecraft.simplekits.def.impl.FactoryConstants;
import org.tpc.minecraft.simplekits.def.impl.materials.MaterialDef.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class MaterialDefTypeAdapter extends TypeAdapter<MaterialDef> {
	
	@Override
	public void write(JsonWriter out, MaterialDef value) throws IOException { }
	
	@Override
	public MaterialDef read(JsonReader in) throws IOException {
		JsonElement element = Streams.parse(in);
		if(!element.isJsonObject()) {
			return null;
		}
		JsonObject object = element.getAsJsonObject();
		MaterialDef material = new MaterialDef();
		
		if(object.has(FactoryConstants.MATERIAL_ID)) {
			JsonElement jsonElement = object.get(FactoryConstants.MATERIAL_ID);
			try {
				material.setId(jsonElement.getAsString());
			} catch(ClassCastException | IllegalStateException e) {
				return null;
			}
		} else {
			return null;
		}
		
		if(object.has(FactoryConstants.MATERIAL_TYPE)) {
			JsonElement jsonElement = object.get(FactoryConstants.MATERIAL_TYPE);
			try {
				String typeName = jsonElement.getAsString();
				for(Type type : Type.values()) {
					if(type.name().equalsIgnoreCase(typeName)) {
						material.setType(type);
						break;
					}
				}
			} catch(ClassCastException | IllegalStateException e) {
				return null;
			}
		}
		
		if(material.getType() == null) {
			material.setType(MaterialDef.Type.Material);
		}
		
		if(object.has(FactoryConstants.MATERIAL_ALIASES)) {
			JsonElement jsonElement = object.get(FactoryConstants.MATERIAL_ALIASES);
			try {
				JsonArray jsonArray = jsonElement.getAsJsonArray();
				for(JsonElement subElement : jsonArray) {
					try {
						material.addAlias(subElement.getAsString());
					} catch(ClassCastException | IllegalStateException e) { }
				}
			} catch(ClassCastException | IllegalStateException e) {
				return null;
			}
		}
		
		if(object.has(FactoryConstants.MATERIAL_DAMAGE)) {
			JsonElement jsonElement = object.get(FactoryConstants.MATERIAL_DAMAGE);
			try {
				int damage = jsonElement.getAsInt();
				if(damage < 0 || damage > Short.MAX_VALUE) {
					return null;
				}
				material.setDamage(damage);
			} catch(ClassCastException | IllegalStateException e) {
				return null;
			}
		}
		
		return material;
	}
}