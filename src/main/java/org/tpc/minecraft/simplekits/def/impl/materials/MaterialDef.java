package org.tpc.minecraft.simplekits.def.impl.materials;

import java.util.ArrayList;
import java.util.List;

public class MaterialDef {
	
	public static enum Type {
		Material, Enchantable, Potion
	}
	
	private String id;
	private Type type;
	private int damage;
	private List<String> aliases;
	
	public MaterialDef() {
		aliases = new ArrayList<>();
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
	
	public void addAlias(String alias) {
		aliases.add(alias);
	}
	
	public void removeAlias(String alias) {
		aliases.remove(alias);
	}
	
	public List<String> getAliases() {
		return aliases;
	}
	
	public void setAliases(List<String> aliases) {
		this.aliases = aliases;
	}
	
	public boolean isEnchantable() {
		return (getType() == Type.Enchantable);
	}
	
	public int getDamage() {
		return damage;
	}
	
	public void setDamage(int damage) {
		this.damage = damage;
	}
}