package org.tpc.minecraft.simplekits.def;

import org.tpc.minecraft.simplekits.kit.IAmount;
import org.tpc.minecraft.simplekits.kit.IMaterial;

public interface IAmountFactory extends IFactory<IAmount, String> {
	
	public IAmount get(long amount);
	public IAmount getDefault(IMaterial material);
	
}