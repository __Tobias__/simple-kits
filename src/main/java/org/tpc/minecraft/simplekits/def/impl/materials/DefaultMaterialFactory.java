package org.tpc.minecraft.simplekits.def.impl.materials;

import static org.tpc.minecraft.simplekits.Constants.DefaultResources.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.def.IMaterialFactory;
import org.tpc.minecraft.simplekits.kit.IMaterial;
import org.tpc.minecraft.simplekits.kit.impl.Material;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DefaultMaterialFactory implements IMaterialFactory {
	
	private Map<String, IMaterial> materials;
	private Map<String, IMaterial> aliases;
	private Gson gson;
	
	private SimpleKits plugin;
	
	public DefaultMaterialFactory(SimpleKits plugin) {
		this.plugin = plugin;
		
		gson = (new GsonBuilder().registerTypeAdapter(TypeToken.of(MaterialDef.class).getType(), new MaterialDefTypeAdapter()).create());
	}
	
	public void load(Path configPath) {
		materials = new TreeMap<>();
		aliases = new TreeMap<>();
		
		if(Files.exists(configPath)) {
			loadFromFile(configPath);
		} else {
			loadDefaults();
		}
	}
	
	private void loadDefaults() {
		try(InputStream in = plugin.getResource(DEFAULTS_MATERIALS);
				Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
				BufferedReader bufferedReader = new BufferedReader(reader)) {
			load(bufferedReader);
		} catch (IOException e) {
			plugin.getLogger().log(Level.WARNING, "Could not load plugin defaults (materials)!");
		}
	}
	
	private void loadFromFile(Path configFile) {
		try(BufferedReader in = Files.newBufferedReader(configFile, StandardCharsets.UTF_8)) {
			load(in);
		} catch (IOException e) {
			plugin.getLogger().log(Level.WARNING, "Could not load plugin configuration (materials)! Loading defaults!");
			
			loadDefaults();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void load(Reader in) {
		load((Map<String, MaterialDef>)gson.fromJson(in, new TypeToken<ConcurrentHashMap<String, MaterialDef>>() {
			private static final long serialVersionUID = 2590224112919043729L;
		}.getType()));
	}
	
	private void load(Map<String, MaterialDef> map) {
		for(Map.Entry<String, MaterialDef> materialEntry : map.entrySet()) {
			String key = materialEntry.getKey();
			MaterialDef material = materialEntry.getValue();
			
			org.bukkit.Material bukkitMaterial = org.bukkit.Material.matchMaterial(material.getId());
			if(bukkitMaterial == null) {
				plugin.getLogger().log(Level.WARNING, "Could not load material definition for '" + material.getId() + "'!");
				return;
			}
			
			boolean enchantable = material.isEnchantable();
			
			materials.put(key.toLowerCase(), new Material(bukkitMaterial, enchantable, key, material.getDamage()));
			
			for(String alias : material.getAliases()) {
				aliases.put(alias.toLowerCase(), new Material(bukkitMaterial, enchantable, alias, material.getDamage()));
			}
		}
	}
	
	@Override
	public IMaterial get(String index) {
		if(materials.containsKey(index = index.toLowerCase())) {
			return materials.get(index);
		}
		
		return aliases.get(index);
	}
	
	@Override
	public String getIndex(IMaterial value) {
		String name = value.getName();
		if(name != null) {
			return name;
		}
		
		String backup = null;
		for(Entry<String, IMaterial> material : materials.entrySet()) {
			if(material.getValue().equals(value)) {
				if(material.getValue().getDamage() == value.getDamage()) {
					return material.getKey();
				}
				
				backup = material.getKey();
			}
		}
		
		return backup;
	}
}