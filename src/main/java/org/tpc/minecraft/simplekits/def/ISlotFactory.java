package org.tpc.minecraft.simplekits.def;

import org.tpc.minecraft.simplekits.kit.ISlot;

public interface ISlotFactory extends IFactory<ISlot, String> { }