package org.tpc.minecraft.simplekits.def;

import org.tpc.minecraft.simplekits.kit.IEnchantment;

public interface IEnchantmentFactory extends IFactory<IEnchantment, String> { }