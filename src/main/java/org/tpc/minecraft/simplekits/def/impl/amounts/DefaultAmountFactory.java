package org.tpc.minecraft.simplekits.def.impl.amounts;

import static org.tpc.minecraft.simplekits.Constants.Configuration.*;
import static org.tpc.minecraft.simplekits.Constants.DefaultResources.*;
import static org.tpc.minecraft.simplekits.Constants.Defaults.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import org.bukkit.configuration.ConfigurationSection;
import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.def.IAmountFactory;
import org.tpc.minecraft.simplekits.kit.IAmount;
import org.tpc.minecraft.simplekits.kit.IMaterial;
import org.tpc.minecraft.simplekits.kit.impl.Amount;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DefaultAmountFactory implements IAmountFactory {
	
	private Map<String, IAmount> amounts;
	private Map<Long, IAmount> numberedAmounts;
	private Gson gson;
	
	private SimpleKits plugin;
	
	public DefaultAmountFactory(SimpleKits plugin) {
		this.plugin = plugin;
		
		gson = (new GsonBuilder().registerTypeAdapter(TypeToken.of(AmountDef.class).getType(), new AmountDefTypeAdapter()).create());
	}
	
	public void load(Path configPath) {
		amounts = new TreeMap<>();
		numberedAmounts = new ConcurrentHashMap<>();
		
		ConfigurationSection section = plugin.getConfiguration().getFeature(CONFIG_FEATURE_STACKS);
		if(section != null) {
			if(section.getBoolean(CONFIG_FEATURE_STACKS_ENABLE, FEATURE_STACKS_ENABLE)) {
				defineStacks();
			}
		}
		
		if(Files.exists(configPath)) {
			loadFromFile(configPath);
		} else {
			loadDefaults();
		}
	}
	
	private void loadDefaults() {
		try(InputStream in = plugin.getResource(DEFAULTS_AMOUNTS);
				Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
				BufferedReader bufferedReader = new BufferedReader(reader)) {
			load(bufferedReader);
		} catch (IOException e) {
			plugin.getLogger().log(Level.WARNING, "Could not load plugin defaults (amounts)!");
		}
	}
	
	private void loadFromFile(Path configFile) {
		try(BufferedReader in = Files.newBufferedReader(configFile, StandardCharsets.UTF_8)) {
			load(in);
		} catch (IOException e) {
			plugin.getLogger().log(Level.WARNING, "Could not load plugin configuration (amounts)! Loading defaults!");
			
			loadDefaults();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void load(Reader in) {
		load((Map<String, AmountDef>)gson.fromJson(in, new TypeToken<ConcurrentHashMap<String, AmountDef>>() {
			private static final long serialVersionUID = -7899553114570003405L;
		}.getType()));
	}
	
	private void load(Map<String, AmountDef> map) {
		for(Map.Entry<String, AmountDef> amount : map.entrySet()) {
			String key = amount.getKey();
			AmountDef amountValue = amount.getValue();
			long amountSize = amountValue.getAmount();
			
			amounts.put(key.toLowerCase(), new Amount(amountSize, key));
			
			for(String alias : amountValue.getAliases()) {
				amounts.put(alias.toLowerCase(), new Amount(amountSize, alias));
			}
		}
	}
	
	@Override
	public IAmount get(String index) {
		if(amounts.containsKey(index = index.toLowerCase())) {
			return amounts.get(index);
		}
		
		try {
			Long amount = Long.valueOf(index);
			if(numberedAmounts.containsKey(amount)) {
				return numberedAmounts.get(amount);
			}
			
			if(!isValidAmount(amount)) {
				return null;
			}
			
			IAmount iAmount = new Amount(amount);
			numberedAmounts.put(amount, iAmount);
			return iAmount;
		} catch(NumberFormatException e) {}
		
		return null;
	}
	
	@Override
	public IAmount get(long amount) {
		if(numberedAmounts.containsKey(amount)) {
			return numberedAmounts.get(amount);
		}
		
		if(!isValidAmount(amount)) {
			return null;
		}
		
		IAmount iAmount = new Amount(amount);
		numberedAmounts.put(amount, iAmount);
		return iAmount;
	}
	
	private boolean isValidAmount(Long amount) {
		if(amount == null) {
			return false;
		}
		
		return (amount.longValue() > 0);
	}
	
	@Override
	public String getIndex(IAmount value) {
		String name = value.getName();
		return ((name != null) ? name : String.valueOf(value.getValue(null))); // Stack-Amounts have always a name
	}
	
	@Override
	public IAmount getDefault(IMaterial material) {
		if((material == null) || (material.getMaterial() == null)) {
			return get(1);
		}
		
		return get(material.getMaterial().getMaxStackSize());
	}
	
	private void defineStacks() {
		final StringBuilder sb = new StringBuilder(50);
		
		for(long size = AMOUNTS_MAX_STACKS; size >= 0; size--) {
			defineNormalStack(sb, size);
		}
		
		for(long fractial = 2; fractial <= AMOUNTS_MAX_FRACTIAL_AMOUNTS; fractial++) {
			defineFractialStack(sb, fractial);
		}
	}
	
	private void defineNormalStack(final StringBuilder sb, final long size) {
		sb.setLength(0); // Reset StringBuilder
		
		sb.append(String.valueOf(size + 1)).append(" stack");
		
		if(size > 0) {
			sb.append('s');
		}
		
		final String finalName = sb.toString();
		amounts.put(finalName, new IAmount() {
			@Override
			public long getValue(IMaterial material) {
				return material.getMaterial().getMaxStackSize() * (size + 1);
			}
			
			@Override
			public String getName() {
				return finalName;
			}
		});
	}
	
	private void defineFractialStack(final StringBuilder sb, final long fractial) {
		for(long value = 1; value < fractial; value += 2) {
			sb.setLength(0); // Reset StringBuilder
			
			sb.append(String.valueOf(value))
				.append('/')
				.append(String.valueOf(fractial))
				.append(" stack");
			
			final String finalName = sb.toString();
			final long finalValue = value;
			if(!amounts.containsKey(finalName)) {
				amounts.put(finalName, new IAmount() {
					@Override
					public long getValue(IMaterial material) {
						return Math.max(material.getMaterial().getMaxStackSize()
								* finalValue / fractial, 1);
					}
					
					@Override
					public String getName() {
						return finalName;
					}
				});
			}
		}
	}
}