package org.tpc.minecraft.simplekits.def.impl.slots;

import java.io.IOException;

import org.tpc.minecraft.simplekits.def.impl.FactoryConstants;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class SlotDefTypeAdapter extends TypeAdapter<SlotDef> {
	@Override
	public void write(JsonWriter out, SlotDef value) throws IOException { }
	
	@Override
	public SlotDef read(JsonReader in) throws IOException {
		JsonElement element = Streams.parse(in);
		if(!element.isJsonObject()) {
			return null;
		}
		JsonObject object = element.getAsJsonObject();
		SlotDef slot = new SlotDef();
		
		if(object.has(FactoryConstants.SLOT_SLOT)) {
			JsonElement jsonElement = object.get(FactoryConstants.SLOT_SLOT);
			try {
				slot.setSlot(jsonElement.getAsLong());
			} catch(ClassCastException | IllegalStateException e) {
				return null;
			}
		}
		
		if(object.has(FactoryConstants.SLOT_ALIASES)) {
			JsonElement jsonElement = object.get(FactoryConstants.SLOT_ALIASES);
			try {
				JsonArray jsonArray = jsonElement.getAsJsonArray();
				for(JsonElement subElement : jsonArray) {
					try {
						slot.addAlias(subElement.getAsString());
					} catch(ClassCastException | IllegalStateException e) { }
				}
			} catch(ClassCastException | IllegalStateException e) {
				return null;
			}
		}
		
		return slot;
	}
}