package org.tpc.minecraft.simplekits.def.impl.slots;

import static org.tpc.minecraft.simplekits.Constants.DefaultResources.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.def.ISlotFactory;
import org.tpc.minecraft.simplekits.kit.ISlot;
import org.tpc.minecraft.simplekits.kit.impl.Slot;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DefaultSlotFactory implements ISlotFactory {
	
	private Map<String, ISlot> slots;
	private Map<Long, ISlot> numberedSlots;
	private Gson gson;
	
	private SimpleKits plugin;
	
	public DefaultSlotFactory(SimpleKits plugin) {
		this.plugin = plugin;
		
		gson = (new GsonBuilder().registerTypeAdapter(TypeToken.of(SlotDef.class).getType(), new SlotDefTypeAdapter()).create());
	}
	
	public void load(Path configPath) {
		slots = new TreeMap<>();
		numberedSlots = new ConcurrentHashMap<>();
		
		if(Files.exists(configPath)) {
			loadFromFile(configPath);
		} else {
			loadDefaults();
		}
	}
	
	private void loadDefaults() {
		try(InputStream in = plugin.getResource(DEFAULTS_SLOTS);
				Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
				BufferedReader bufferedReader = new BufferedReader(reader)) {
			load(bufferedReader);
		} catch (IOException e) {
			plugin.getLogger().log(Level.WARNING, "Could not load plugin defaults (slots)!");
		}
	}
	
	private void loadFromFile(Path configFile) {
		try(BufferedReader in = Files.newBufferedReader(configFile, StandardCharsets.UTF_8)) {
			load(in);
		} catch (IOException e) {
			plugin.getLogger().log(Level.WARNING, "Could not load plugin configuration (slots)! Loading defaults!");
			
			loadDefaults();
		}
	}

	@SuppressWarnings("unchecked")
	private void load(Reader in) {
		load((Map<String, SlotDef>)gson.fromJson(in, new TypeToken<ConcurrentHashMap<String, SlotDef>>() {
			private static final long serialVersionUID = -8630253488475689248L;
		}.getType()));
	}
	
	private void load(Map<String, SlotDef> map) {
		for(Map.Entry<String, SlotDef> slot : map.entrySet()) {
			String key = slot.getKey();
			SlotDef slotValue = slot.getValue();
			long slotIndex = slotValue.getSlot();
			
			slots.put(key.toLowerCase(), new Slot(slotIndex, key));
			
			for(String alias : slotValue.getAliases()) {
				slots.put(alias.toLowerCase(), new Slot(slotIndex, alias));
			}
		}
	}
	
	@Override
	public ISlot get(String index) {
		if(slots.containsKey(index = index.toLowerCase())) {
			return slots.get(index);
		}
		
		try {
			Long slot = Long.valueOf(index);
			if(numberedSlots.containsKey(slot)) {
				return numberedSlots.get(slot);
			}
			
			if(!isValidSlot(slot)) {
				return null;
			}
			
			ISlot iSlot = new Slot(slot);
			numberedSlots.put(slot, iSlot);
			return iSlot;
		} catch(NumberFormatException e) {}
		
		return null;
	}
	
	private boolean isValidSlot(Long slot) {
		if(slot == null) {
			return false;
		}
		
		long value = slot.longValue();
		return ((value >= 0) && (value < 36))
				|| ((value >= 100) && (value < 104));
	}

	@Override
	public String getIndex(ISlot value) {
		String name = value.getName();
		if(name != null) {
			return name;
		}
		
		return String.valueOf(value.getSlot());
	}
}