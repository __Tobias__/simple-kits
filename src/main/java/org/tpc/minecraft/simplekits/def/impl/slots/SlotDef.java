package org.tpc.minecraft.simplekits.def.impl.slots;

import java.util.ArrayList;
import java.util.List;

public class SlotDef {
	
	private List<String> aliases;
	private long slot;
	
	public SlotDef() {
		aliases = new ArrayList<>();
	}
	
	public List<String> getAliases() {
		return aliases;
	}
	
	public void addAlias(String name) {
		aliases.add(name);
	}
	
	public void removeAlias(String name) {
		aliases.remove(name);
	}
	
	public void setAliases(List<String> aliases) {
		this.aliases = aliases;
	}
	
	public long getSlot() {
		return slot;
	}
	
	public void setSlot(long slot) {
		this.slot = slot;
	}
}