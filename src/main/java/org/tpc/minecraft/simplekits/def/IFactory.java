package org.tpc.minecraft.simplekits.def;

import java.nio.file.Path;

public interface IFactory<Type, I> {
	
	public void load(Path configPath);
	public Type get(I index);
	public I getIndex(Type value);
	
}