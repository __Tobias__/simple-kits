package org.tpc.minecraft.simplekits.def;

import org.tpc.minecraft.simplekits.kit.IMaterial;

public interface IMaterialFactory extends IFactory<IMaterial, String> { }