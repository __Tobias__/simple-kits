package org.tpc.minecraft.simplekits.def.impl.enchantments;

import static org.tpc.minecraft.simplekits.Constants.DefaultResources.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import org.bukkit.enchantments.EnchantmentWrapper;
import org.tpc.minecraft.simplekits.SimpleKits;
import org.tpc.minecraft.simplekits.def.IEnchantmentFactory;
import org.tpc.minecraft.simplekits.kit.IEnchantment;
import org.tpc.minecraft.simplekits.kit.impl.Enchantment;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DefaultEnchantmentFactory implements IEnchantmentFactory {
	
//	private static final IEnchantment clone(IEnchantment enchantment) {
//		return new Enchantment(enchantment.getEnchantment(), enchantment.getName());
//	}
	
	private Map<String, IEnchantment> enchantments;
	private Map<String, IEnchantment> aliases;
	
	private Gson gson;
	
	private SimpleKits plugin;
	
	public DefaultEnchantmentFactory(SimpleKits plugin) {
		this.plugin = plugin;
		
		gson = (new GsonBuilder().registerTypeAdapter(TypeToken.of(EnchantmentDef.class).getType(), new EnchantmentDefTypeAdapter()).create());
	}
	
	public void load(Path configPath) {
		enchantments = new TreeMap<>();
		aliases = new TreeMap<>();
		
		if(Files.exists(configPath)) {
			loadFromFile(configPath);
		} else {
			loadDefaults();
		}
	}
	
	private void loadDefaults() {
		try(InputStream in = plugin.getResource(DEFAULTS_ENCHANTMENTS);
				Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
				BufferedReader bufferedReader = new BufferedReader(reader)) {
			load(bufferedReader);
		} catch (IOException e) {
			plugin.getLogger().log(Level.WARNING, "Could not load plugin defaults (enchantments)!");
		}
	}
	
	private void loadFromFile(Path configFile) {
		try(BufferedReader in = Files.newBufferedReader(configFile, StandardCharsets.UTF_8)) {
			load(in);
		} catch (IOException e) {
			plugin.getLogger().log(Level.WARNING, "Could not load plugin configuration (materials)! Loading defaults!");
			
			loadDefaults();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void load(Reader in) {
		load((Map<String, EnchantmentDef>)gson.fromJson(in, new TypeToken<ConcurrentHashMap<String, EnchantmentDef>>() {
			private static final long serialVersionUID = 2590224112919043729L;
		}.getType()));
	}
	
	private void load(Map<String, EnchantmentDef> map) {
		for(Map.Entry<String, EnchantmentDef> enchantmentEntry : map.entrySet()) {
			String key = enchantmentEntry.getKey();
			EnchantmentDef enchantment = enchantmentEntry.getValue();
			
			org.bukkit.enchantments.Enchantment bukkitEnchantment = EnchantmentWrapper.getByName(enchantment.getId());
			if(bukkitEnchantment == null) {
				plugin.getLogger().log(Level.WARNING, "Could not load enchantment definition for '" + enchantment.getId() + "'!");
				return;
			}
			
			enchantments.put(key.toLowerCase(), new Enchantment(bukkitEnchantment, key));
			
			for(String alias : enchantment.getAliases()) {
				enchantments.put(alias.toLowerCase(), new Enchantment(bukkitEnchantment, alias));
			}
		}
	}
	
	@Override
	public IEnchantment get(String index) {
		if(enchantments.containsKey(index = index.toLowerCase())) {
			return enchantments.get(index);
		}
		
		return aliases.get(index);
	}
	
	@Override
	public String getIndex(IEnchantment value) {
		String name = value.getName();
		if(name != null) {
			return name;
		}
		
		for(Entry<String, IEnchantment> enchantment : enchantments.entrySet()) {
			if(enchantment.getValue().equals(value)) {
				return enchantment.getKey();
			}
		}
		
		return null;
	}
}