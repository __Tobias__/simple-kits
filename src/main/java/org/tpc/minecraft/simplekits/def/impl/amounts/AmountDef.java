package org.tpc.minecraft.simplekits.def.impl.amounts;

import java.util.ArrayList;
import java.util.List;

public class AmountDef {
	
	private List<String> aliases;
	private long amount;
	
	public AmountDef() {
		aliases = new ArrayList<>();
	}
	
	public List<String> getAliases() {
		return aliases;
	}
	
	public void addAlias(String name) {
		aliases.add(name);
	}
	
	public void removeAlias(String name) {
		aliases.remove(name);
	}
	
	public void setAliases(List<String> aliases) {
		this.aliases = aliases;
	}
	
	public long getAmount() {
		return amount;
	}
	
	public void setAmount(long amount) {
		this.amount = amount;
	}
}