package org.tpc.minecraft.simplekits.data;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.avaje.ebean.validation.NotNull;

@Entity
@Table(name = "ignores")
public class Ignore implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8629043752412529113L;
	
	@Id
	@NotNull
	@Column(name = "id")
	private UUID id;
	
	
	@NotNull
	@Column(name = "kit")
	private String kit;
	
	public Ignore() { }
	
	public Ignore(String kit) {
		this.kit = kit;
	}
	
	public UUID getId() {
		return id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}
	
	public String getKit() {
		return kit;
	}
	
	public void setKit(String kit) {
		this.kit = kit;
	}
}