package org.tpc.minecraft.simplekits.data;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.avaje.ebean.validation.NotNull;

@Entity
@Table(name = "cooldowns")
public class Cooldown implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8539750827718158911L;
	
	@Id
	@NotNull
	@Column(name = "id")
	private UUID id;
	
	@NotNull
	@Column(name = "player")
	private UUID player;
	
	@NotNull
	@Column(name = "kit")
	private String kitName;
	
	@NotNull
	@Column(name = "time")
	private long creationTime;
	
	public Cooldown() { }
	
	public Cooldown(UUID player, String kit) {
		this.player = player;
		this.kitName = kit;
		this.creationTime = System.currentTimeMillis();
	}
	
	public UUID getId() {
		return id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}
	
	public UUID getPlayer() {
		return player;
	}
	
	public void setPlayer(UUID player) {
		this.player = player;
	}
	
	public String getKitName() {
		return kitName;
	}
	
	public void setKitName(String kitName) {
		this.kitName = kitName;
	}
	
	public long getCreationTime() {
		return creationTime;
	}
	
	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}
}