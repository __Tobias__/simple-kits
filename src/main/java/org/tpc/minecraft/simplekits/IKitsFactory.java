package org.tpc.minecraft.simplekits;

public interface IKitsFactory {
	
	public IKits load();
	
}