package org.tpc.minecraft.simplekits;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class GlobalMessages {
	
	private static final String BUNDLE_NAME = "messages.messages"; //$NON-NLS-1$
	private static ResourceBundle RESOURCE_BUNDLE;
	
	static {
		try {
			RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
		} catch(MissingResourceException resourceException) {
			System.err.println("Simple-Kits: Could not load text-resources!"); //$NON-NLS-1$
		}
	}
	
	private GlobalMessages() { }
	
	public static String getString(String key, Object ... args) {
		if(RESOURCE_BUNDLE == null) {
			return '!' + key + '!';
		}
		
		try {
			return MessageFormat.format(RESOURCE_BUNDLE.getString(key), args);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}