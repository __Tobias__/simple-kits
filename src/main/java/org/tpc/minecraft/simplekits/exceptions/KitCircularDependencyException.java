package org.tpc.minecraft.simplekits.exceptions;

public class KitCircularDependencyException extends KitDependencyException {
	
	private static final long serialVersionUID = -1398933404657566901L;
	
	public KitCircularDependencyException(String kitName, String dependencyName, boolean baseReference) {
		super(kitName, dependencyName, baseReference);
	}
}