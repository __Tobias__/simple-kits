package org.tpc.minecraft.simplekits.exceptions;

public class KitDependencyNotFoundException extends KitDependencyException {
	
	private static final long serialVersionUID = -315590487526544345L;
	
	public KitDependencyNotFoundException(String kitName, String dependencyName, boolean baseReference) {
		super(kitName, dependencyName, baseReference);
	}
	
}