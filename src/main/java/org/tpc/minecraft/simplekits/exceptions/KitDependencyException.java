package org.tpc.minecraft.simplekits.exceptions;

public class KitDependencyException extends RuntimeException {
	
	private static final long serialVersionUID = 6532718442449935508L;
	
	private String kitName;
	private String dependencyName;
	private boolean baseReference;
	
	public KitDependencyException(String kitName, String dependencyName) {
		this.kitName = kitName;
		this.dependencyName = dependencyName;
	}
	
	public KitDependencyException(String kitName, String dependencyName, boolean baseReference) {
		this.kitName = kitName;
		this.dependencyName = dependencyName;
		this.baseReference = baseReference;
	}
	
	public String getKitName() {
		return kitName;
	}
	
	public String getDependencyName() {
		return dependencyName;
	}
	
	public boolean isBaseReference() {
		return baseReference;
	}
}