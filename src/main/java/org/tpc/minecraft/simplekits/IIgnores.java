package org.tpc.minecraft.simplekits;

public interface IIgnores extends ILoadable {
	
	public void store(String kit);
	public boolean isIgnored(String kit);
	public void remove(String kit);
	
}