package org.tpc.minecraft.simplekits.conf.adapter;

import static org.tpc.minecraft.simplekits.Constants.DataModel.*;
import static org.tpc.minecraft.simplekits.conf.adapter.KitTypeAdapter.*;

import java.io.IOException;

import org.tpc.minecraft.simplekits.conf.impl.ComponentConf;
import org.tpc.minecraft.simplekits.conf.impl.ItemConf;
import org.tpc.minecraft.simplekits.conf.impl.ReferenceConf;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Streams;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class ComponentTypeAdapter extends TypeAdapter<ComponentConf> {
	
	private TypeAdapter<ItemConf> itemAdabter;
	
	public ComponentTypeAdapter() { }
	
	public void setGson(Gson gson) {
		itemAdabter = gson.getAdapter(TypeToken.get(ItemConf.class));
	}
	
	@Override
	public void write(JsonWriter out, ComponentConf value) throws IOException {
		JsonElement element = null;
		
		if(value instanceof ItemConf){
			element = itemAdabter.toJsonTree((ItemConf)value);
		} else if(value instanceof ReferenceConf) {
			ReferenceConf reference = (ReferenceConf) value;
			
			JsonObject referenceObject = new JsonObject();
			switch(reference.getType()) {
			case Kit:
				referenceObject.addProperty(REFERENCE_KIT_NAME, reference.getName());
				break;
			case Template:
				referenceObject.addProperty(REFERENCE_TEMPLATE_NAME, reference.getName());
				break;
			case Reference:
			default:
				referenceObject.addProperty(REFERENCE_REFERENCE_NAME, reference.getName());
				break;
			}
			
			element = referenceObject;
		} else {
			return;
		}
		
		Streams.write(element, out);
	}
	
	@Override
	public ComponentConf read(JsonReader in) throws IOException {
		JsonElement element = Streams.parse(in);
		if(element.isJsonPrimitive()) {
			try {
				String name = element.getAsString();
				if(name.startsWith(REFERENCE_KIT_NAME + ":")) {
					return ReferenceConf.kit(name.substring(REFERENCE_KIT_NAME.length() + 1));
				} else if(name.startsWith(REFERENCE_TEMPLATE_NAME + ":")) {
					return ReferenceConf.template(name.substring(REFERENCE_TEMPLATE_NAME.length() + 1));
				} else {
					for(String refName : REFERENCE_NAMES) {
						if(name.startsWith(refName + ":")) {
							return ReferenceConf.reference(name.substring(refName.length() + 1));
						}
					}
				}
				
				return ReferenceConf.reference(name);
			} catch(IllegalArgumentException | ClassCastException e) {
				throw createJSonException(REFERENCE_NAME, KIT_ITEMS_NAME, element);
			}
		}
		
		try {
			JsonObject childJsonObject = element.getAsJsonObject();
			if(childJsonObject.has(REFERENCE_KIT_NAME)) {
				try {
					return ReferenceConf.kit(childJsonObject.get(REFERENCE_KIT_NAME).getAsString());
				} catch(ClassCastException | IllegalStateException e) {
					throw createJSonException(REFERENCE_KIT_NAME, KIT_ITEMS_NAME, childJsonObject);
				}
			}
			if(childJsonObject.has(REFERENCE_TEMPLATE_NAME)) {
				try {
					return ReferenceConf.kit(childJsonObject.get(REFERENCE_TEMPLATE_NAME).getAsString());
				} catch(ClassCastException | IllegalStateException e) {
					throw createJSonException(REFERENCE_TEMPLATE_NAME, KIT_ITEMS_NAME, childJsonObject);
				}
			}
			
			for(String name : REFERENCE_NAMES) {
				if(childJsonObject.has(name)) {
					try {
						return ReferenceConf.reference(childJsonObject.get(name).getAsString());
					} catch(ClassCastException | IllegalStateException e) {
						throw createJSonException(REFERENCE_NAME, KIT_ITEMS_NAME, childJsonObject);
					}
				}
			}
			
			ItemConf item = itemAdabter.fromJsonTree(childJsonObject);
			if(item == null) {
				throw createJSonException(KIT_ITEM_NAME, KIT_ITEMS_NAME, childJsonObject);
			}
			
			return item;
		} catch(ClassCastException | IllegalStateException e) {
			throw createJSonException(KIT_ITEM_NAME, KIT_ITEMS_NAME, element);
		}
	}
}