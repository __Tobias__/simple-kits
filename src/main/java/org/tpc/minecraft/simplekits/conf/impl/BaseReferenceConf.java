package org.tpc.minecraft.simplekits.conf.impl;

import static org.tpc.minecraft.simplekits.Constants.DataModel.*;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseReferenceConf {
	
	public static enum Type {
		Reference, Kit, Template
	}
	
	public static BaseReferenceConf reference(String name) {
		return new BaseReferenceConf(name, Type.Reference);
	}
	
	public static BaseReferenceConf kit(String name) {
		return new BaseReferenceConf(name, Type.Kit);
	}
	
	public static BaseReferenceConf template(String name) {
		return new BaseReferenceConf(name, Type.Template);
	}
	
	@Expose
	@SerializedName(BASE_PRIORITY_NAME)
	private long priority;
	
	@Expose
	@SerializedName(BASE_NAME_NAME)
	private String name;
	
	@Expose
	@SerializedName(BASE_TYPE_NAME)
	private Type type;
	
	public BaseReferenceConf(String name, Type type) {
		this.name = name;
		this.type = type;
	}
	
	public long getPriority() {
		return priority;
	}
	
	public void setPriority(long priority) {
		this.priority = priority;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isBasic() {
		return priority == 0;
	}
	
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
}