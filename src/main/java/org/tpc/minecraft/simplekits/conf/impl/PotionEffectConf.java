package org.tpc.minecraft.simplekits.conf.impl;

import static org.tpc.minecraft.simplekits.Constants.DataModel.*;

import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.tpc.minecraft.simplekits.kit.IPotionEffect;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PotionEffectConf implements IPotionEffect {
	
	@Expose
	@SerializedName(POTION_EFFECT_TYPE_NAME)
	private PotionEffectType potionType;
	
	@Expose
	@SerializedName(POTION_EFFECT_AMPLIFIER_NAME)
	private int amplifier;
	
	@Expose
	@SerializedName(POTION_EFFECT_DURATION_NAME)
	private int duration;
	
	
	@Expose
	@SerializedName(POTION_EFFECT_SPLASH_NAME)
	private boolean splash;
	
	public PotionEffectConf() {
		amplifier = 1;
		duration = 600;
		
		splash = false;
	}
	
	public PotionEffectConf(PotionEffectType effectType) {
		this();
		this.potionType = effectType;
	}
	
	public PotionEffectConf(PotionEffect effect) {
		potionType = effect.getType();
		amplifier = effect.getAmplifier();
		duration = effect.getDuration();
		
		splash = effect.isAmbient();
	}
	
	public void setAmbient(boolean ambient) {
		this.splash = ambient;
	}
	
	public void setAmplifier(int amplifier) {
		this.amplifier = amplifier;
	}
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public void setPotionType(PotionEffectType potionType) {
		this.potionType = potionType;
	}
	
	@Override
	public PotionEffectType getType() {
		return potionType;
	}
	
	@Override
	public int getDuration() {
		return duration;
	}
	
	@Override
	public int getAmplifier() {
		return amplifier;
	}
	
	@Override
	public boolean isSplash() {
		return splash;
	}
}