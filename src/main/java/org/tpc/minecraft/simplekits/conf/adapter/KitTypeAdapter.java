package org.tpc.minecraft.simplekits.conf.adapter;

import static org.tpc.minecraft.simplekits.Constants.DataModel.*;

import java.io.IOException;

import org.tpc.minecraft.simplekits.conf.impl.BaseReferenceConf;
import org.tpc.minecraft.simplekits.conf.impl.ComponentConf;
import org.tpc.minecraft.simplekits.conf.impl.KitConf;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Streams;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class KitTypeAdapter extends TypeAdapter<KitConf> {
	
	public static JsonSyntaxException createJSonException(String name, String property, JsonElement element) {
		return createJSonException(name, property, element.toString());
	}
	
	public static JsonSyntaxException createJSonException(String name, String property, String message) {
		return new JsonSyntaxException("Invalid element found as " + name + " (" + KIT_NAME + "->" + property + "): " + message);
	}
	
	public static JsonSyntaxException createJSonException(JsonElement element) {
		return new JsonSyntaxException("Invalid element found as " + KIT_NAME + " (" + KIT_NAME + "): " + element.toString());
	}
	
	private TypeAdapter<ComponentConf> itemAdabter;
	private TypeAdapter<BaseReferenceConf> baseAdabter;
	
	public KitTypeAdapter() { }
	
	public void setGson(Gson gson) {
		itemAdabter = gson.getAdapter(TypeToken.get(ComponentConf.class));
		baseAdabter = gson.getAdapter(TypeToken.get(BaseReferenceConf.class));
	}
	
	@Override
	public void write(JsonWriter out, KitConf value) throws IOException {
		if(value == null){
			return;
		}
		
		JsonObject kitObject = new JsonObject();
		
		JsonArray itemList = new JsonArray();
		for(ComponentConf item : value.getItems()) {
			itemList.add(itemAdabter.toJsonTree(item));
		}
		kitObject.add(KIT_ITEMS_NAME, itemList);
		
		JsonArray aliasList = new JsonArray();
		for(String alias : value.getAliases()){
			aliasList.add(new JsonPrimitive(alias));
		}
		kitObject.add(KIT_ALIASES_NAME, aliasList);
		
		JsonArray baseList = new JsonArray();
		for(BaseReferenceConf base : value.getBases()){
			baseList.add(baseAdabter.toJsonTree(base));
		}
		kitObject.add(KIT_BASES_NAME, baseList);
		
		if(value.getCooldown() > 0) {
			kitObject.add(KIT_COOLDOWN_NAME, new JsonPrimitive(value.getCooldown()));
		}
		
		Streams.write(kitObject, out);
	}
	
	@Override
	public KitConf read(JsonReader in) throws IOException {
		JsonElement rootElement = Streams.parse(in);
		try {
			JsonObject jsonObject = rootElement.getAsJsonObject();
			
			KitConf kit = new KitConf();
			
			if(jsonObject.has(KIT_ITEMS_NAME)) {
				JsonArray jsonArray = jsonObject.get(KIT_ITEMS_NAME).getAsJsonArray();
				for(JsonElement element : jsonArray) {
					ComponentConf item = itemAdabter.fromJsonTree(element);
					if(item == null) {
						throw createJSonException(KIT_ITEM_NAME, KIT_ITEMS_NAME, element);
					}
					
					kit.addItem(item);
				}
			}
			
			if(jsonObject.has(KIT_ALIASES_NAME)) {
				JsonElement object = jsonObject.get(KIT_ALIASES_NAME);
				
				if(object.isJsonPrimitive()) {
					try {
						kit.addAlias(object.getAsString());
					} catch(ClassCastException | IllegalStateException e) {
						throw createJSonException(KIT_ALIAS_NAME, KIT_ALIASES_NAME, object);
					}
				} else {
					try {
						JsonArray jsonArray = object.getAsJsonArray();
						for(JsonElement element : jsonArray) {
							try {
								kit.addAlias(element.getAsString());
							} catch(ClassCastException | IllegalStateException e) {
								throw createJSonException(KIT_ALIAS_NAME, KIT_ALIASES_NAME, element);
							}
						}
					} catch(ClassCastException | IllegalStateException e) {
						throw createJSonException(KIT_ALIAS_NAME, KIT_ALIASES_NAME, object);
					}
				}
			}
			
			if(jsonObject.has(KIT_COOLDOWN_NAME)) {
				JsonElement object = jsonObject.get(KIT_COOLDOWN_NAME);
				try {
					kit.setCooldown(object.getAsLong());
				} catch(ClassCastException | IllegalStateException e) {
					throw createJSonException(KIT_COOLDOWN_NAME, KIT_COOLDOWN_NAME, object);
				}
			}
			
			if(jsonObject.has(KIT_BASES_NAME)){
				JsonElement object = jsonObject.get(KIT_BASES_NAME);
				
				if(!object.isJsonArray()) {
					BaseReferenceConf base = baseAdabter.fromJsonTree(object);
					if(base == null) {
						throw createJSonException(KIT_BASE_NAME, KIT_BASES_NAME, object);
					}
					
					kit.addBase(base);
				} else {
					try {
						JsonArray jsonArray = object.getAsJsonArray();
						for(JsonElement element : jsonArray) {
							BaseReferenceConf base = baseAdabter.fromJsonTree(element);
							if(base == null) {
								throw createJSonException(KIT_BASE_NAME, KIT_BASES_NAME, element);
							}
							
							kit.addBase(base);
						}
					} catch(ClassCastException | IllegalStateException e) {
						throw createJSonException(KIT_BASE_NAME, KIT_BASES_NAME, object);
					}
				}
			}
			
			return kit;
		} catch(ClassCastException | IllegalStateException e) {
			throw createJSonException(rootElement);
		}
	}
}