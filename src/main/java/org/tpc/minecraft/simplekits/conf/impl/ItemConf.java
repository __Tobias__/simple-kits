package org.tpc.minecraft.simplekits.conf.impl;

import static org.tpc.minecraft.simplekits.Constants.DataModel.*;

import java.util.ArrayList;
import java.util.List;

import org.tpc.minecraft.simplekits.kit.IAmount;
import org.tpc.minecraft.simplekits.kit.IMaterial;
import org.tpc.minecraft.simplekits.kit.IPotionEffect;
import org.tpc.minecraft.simplekits.kit.ISlot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemConf extends ComponentConf {
	
	@Expose
	@SerializedName(ITEM_ID_NAME)
	private IMaterial material;
	
	@Expose
	@SerializedName(ITEM_AMOUNT_NAME)
	private IAmount amount;
	
	@Expose
	@SerializedName(ITEM_SLOT_NAME)
	private ISlot slot;
	
	@Expose
	@SerializedName(POTION_EFFECTS_NAME)
	private List<IPotionEffect> effects;
	
	@Expose
	@SerializedName(ITEM_ENCHANTMENTS_NAME)
	private List<EnchantmentConf> enchantments;
	
	@Expose
	@SerializedName(ITEM_NAME_NAME)
	private String itemName;
	
	@Expose(deserialize = false, serialize = false)
	private boolean potion;
	
	@Expose(deserialize = false, serialize = false)
	private boolean enchanted;
	
	@Expose
	@SerializedName(ITEM_DAMAGE_NAME)
	private int damage;
	
	public ItemConf() {
		effects = new ArrayList<>();
		enchantments = new ArrayList<>();
	}
	
	public IMaterial getId() {
		return material;
	}
	
	public void setId(IMaterial id) {
		this.material = id;
	}
	
	public IAmount getAmount() {
		return amount;
	}
	
	public void setAmount(IAmount amount) {
		this.amount = amount;
	}
	
	public ISlot getSlot() {
		return slot;
	}
	
	public void setSlot(ISlot slot) {
		this.slot = slot;
	}
	
	public void addEffect(IPotionEffect effect) {
		effects.add(effect);
	}
	
	public void removeEffect(IPotionEffect effect) {
		effects.remove(effect);
	}
	
	public void clearEffects() {
		effects.clear();
	}
	
	public List<IPotionEffect> getEffects() {
		return effects;
	}
	
	public void setEffects(List<IPotionEffect> effects) {
		this.effects = effects;
	}
	
	public boolean isPotion() {
		return potion;
	}
	
	public void setPotion(boolean potion) {
		this.potion = potion;
	}
	
	public List<EnchantmentConf> getEnchantments() {
		return enchantments;
	}
	
	public void addEnchantment(EnchantmentConf enchantment) {
		enchantments.add(enchantment);
	}
	
	public void removeEnchantment(EnchantmentConf enchantment) {
		enchantments.remove(enchantment);
	}
	
	public void setEnchantments(List<EnchantmentConf> enchantments) {
		this.enchantments = enchantments;
	}
	
	public String getItemName() {
		return itemName;
	}
	
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public boolean isEnchanted() {
		return enchanted;
	}
	
	public void setEnchanted(boolean enchanted) {
		this.enchanted = enchanted;
	}
	
	public int getDamage() {
		return damage;
	}
	
	public void setDamage(int damage) {
		this.damage = damage;
	}
}