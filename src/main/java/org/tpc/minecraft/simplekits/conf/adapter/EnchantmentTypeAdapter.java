package org.tpc.minecraft.simplekits.conf.adapter;

import static org.tpc.minecraft.simplekits.Constants.DataModel.*;
import static org.tpc.minecraft.simplekits.conf.adapter.KitTypeAdapter.*;

import java.io.IOException;

import org.tpc.minecraft.simplekits.conf.impl.EnchantmentConf;
import org.tpc.minecraft.simplekits.def.IEnchantmentFactory;
import org.tpc.minecraft.simplekits.kit.IEnchantment;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class EnchantmentTypeAdapter extends TypeAdapter<EnchantmentConf> {
	
	private IEnchantmentFactory enchantmentFactory;
	
	public EnchantmentTypeAdapter(IEnchantmentFactory enchantmentFactory) {
		this.enchantmentFactory = enchantmentFactory;
	}
	
	@Override
	public void write(JsonWriter out, EnchantmentConf value) throws IOException {
		if(value == null){
			return;
		}
		JsonObject object = new JsonObject();
		
		if(value.getName() != null) {
			object.addProperty(ENCHANTMENT_TYPE_NAME, value.getName());
		}
		
		object.addProperty(ENCHANTMENT_LEVEL_NAME, value.getLevel());
		
		Streams.write(object, out);
	}
	
	@Override
	public EnchantmentConf read(JsonReader in) throws IOException {
		JsonElement rootElement = Streams.parse(in);
		try {
			JsonObject jsonObject = rootElement.getAsJsonObject();
			
			EnchantmentConf enchantment = new EnchantmentConf();
			
			if(jsonObject.has(ENCHANTMENT_TYPE_NAME)) {
				JsonElement object = jsonObject.get(ENCHANTMENT_TYPE_NAME);
				try {
					IEnchantment iEnchantment = enchantmentFactory.get(object.getAsString());
					if(iEnchantment == null) {
						throw createJSonException(ENCHANTMENT_TYPE_NAME, ENCHANTMENT_TYPE_NAME, object);
					}
					
					enchantment.setType(iEnchantment.getEnchantment());
					enchantment.setName(object.getAsString());
				} catch(ClassCastException | IllegalStateException e) {
					throw createJSonException(ENCHANTMENT_TYPE_NAME, ENCHANTMENT_TYPE_NAME, object);
				}
			} else {
				return null;
			}
			
			if(enchantment.getType() == null) {
				return null;
			}
			
			if(jsonObject.has(ENCHANTMENT_LEVEL_NAME)) {
				JsonElement object = jsonObject.get(ENCHANTMENT_LEVEL_NAME);
				try {
					int level = object.getAsInt();
					if(level < enchantment.getType().getStartLevel() || level > enchantment.getType().getMaxLevel()) {
						throw createJSonException(KIT_ITEM_NAME, KIT_ITEMS_NAME, "Level out of range: " + object.toString());
					}
					
					enchantment.setLevel(level);
				} catch(ClassCastException | IllegalStateException e) {
					throw createJSonException(KIT_ITEM_NAME, KIT_ITEMS_NAME, object);
				}
			}
			
			return enchantment;
		} catch(ClassCastException | IllegalStateException e) {
			throw createJSonException(rootElement);
		}
	}
}