package org.tpc.minecraft.simplekits.conf.adapter.bukkit;

import java.io.IOException;

import org.bukkit.potion.PotionEffectType;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class BukkitPotionEffectTypeAdapter extends TypeAdapter<PotionEffectType> {
	
	@Override
	public void write(JsonWriter out, PotionEffectType value) throws IOException {
		Streams.write(new JsonPrimitive(value.getName()), out);
	}
	
	@Override
	public PotionEffectType read(JsonReader in) throws IOException {
		JsonElement element = Streams.parse(in);
		
		try {
			return PotionEffectType.getByName(element.getAsString());
		} catch(IllegalArgumentException | ClassCastException e) {
			return null;
		}
	}
}