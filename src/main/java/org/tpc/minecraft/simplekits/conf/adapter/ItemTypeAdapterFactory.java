package org.tpc.minecraft.simplekits.conf.adapter;

import static org.tpc.minecraft.simplekits.Constants.DataModel.*;
import static org.tpc.minecraft.simplekits.conf.adapter.KitTypeAdapter.*;

import java.io.IOException;

import org.tpc.minecraft.simplekits.conf.impl.EnchantmentConf;
import org.tpc.minecraft.simplekits.conf.impl.ItemConf;
import org.tpc.minecraft.simplekits.conf.impl.PotionEffectConf;
import org.tpc.minecraft.simplekits.def.IAmountFactory;
import org.tpc.minecraft.simplekits.def.IMaterialFactory;
import org.tpc.minecraft.simplekits.def.ISlotFactory;
import org.tpc.minecraft.simplekits.kit.IAmount;
import org.tpc.minecraft.simplekits.kit.IMaterial;
import org.tpc.minecraft.simplekits.kit.IPotion;
import org.tpc.minecraft.simplekits.kit.IPotionEffect;
import org.tpc.minecraft.simplekits.kit.ISlot;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.internal.Streams;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class ItemTypeAdapterFactory implements TypeAdapterFactory {
	
	private ISlotFactory slotFactory;
	private IMaterialFactory materialFactory;
	private IAmountFactory amountFactory;
	
	public ItemTypeAdapterFactory(ISlotFactory slotFactory, IMaterialFactory materialFactory, IAmountFactory amountFactory) {
		this.slotFactory = slotFactory;
		this.materialFactory = materialFactory;
		this.amountFactory = amountFactory;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
		if(!ItemConf.class.isAssignableFrom(type.getRawType())) {
			return null;
		}
		
		final TypeAdapter<EnchantmentConf> enchantmentAdapter = gson.getAdapter(TypeToken.get(EnchantmentConf.class));
		final TypeAdapter<PotionEffectConf> effectAdapter = gson.getAdapter(TypeToken.get(PotionEffectConf.class));
		
		return (TypeAdapter<T>) new TypeAdapter<ItemConf>() {
			@Override
			public void write(JsonWriter out, ItemConf value) throws IOException {
				JsonObject object = new JsonObject();
				if(value instanceof IPotion) {
					JsonArray jsonArray = new JsonArray();
					
					IPotion potion = (IPotion) value;
					for(IPotionEffect effect : potion.getEffects()) {
						jsonArray.add(effectAdapter.toJsonTree((PotionEffectConf)effect));
					}
					
					object.add(POTION_EFFECT_NAME, jsonArray);
				}
				
				if(!value.getEnchantments().isEmpty()) {
					JsonArray jsonArray = new JsonArray();
					
					for(EnchantmentConf enchantment : value.getEnchantments()) {
						jsonArray.add(enchantmentAdapter.toJsonTree(enchantment));
					}
					
					object.add(ITEM_ENCHANTMENTS_NAME, jsonArray);
				}
				
				object.add(ITEM_ID_NAME, new JsonPrimitive(materialFactory.getIndex(value.getId())));
				
				if(value.getAmount() != null) {
					object.add(ITEM_AMOUNT_NAME, new JsonPrimitive(amountFactory.getIndex(value.getAmount())));
				}
				
				if(value.getSlot() != null) {
					object.add(ITEM_SLOT_NAME, new JsonPrimitive(slotFactory.getIndex(value.getSlot())));
				}
			}
			
			@Override
			public ItemConf read(JsonReader in) throws IOException {
				JsonElement element = Streams.parse(in);
				try {
					JsonObject jsonObject = element.getAsJsonObject();
					ItemConf item = new ItemConf();
					
					if(jsonObject.has(POTION_EFFECTS_NAME) || jsonObject.has(POTION_EFFECT_NAME)) {
						item.setPotion(true);
						
						if(jsonObject.has(POTION_EFFECTS_NAME)) {
							JsonElement jsonElement = jsonObject.get(POTION_EFFECTS_NAME);
							try {
								
								JsonArray jsonArray = jsonElement.getAsJsonArray();
								
								for(JsonElement arrayElement : jsonArray) {
									IPotionEffect effect = effectAdapter.fromJsonTree(arrayElement);
									if(effect == null) {
										throw createJSonException(POTION_EFFECT_NAME, POTION_EFFECTS_NAME, arrayElement);
									}
									
									item.addEffect(effect);
								}
							} catch(ClassCastException | IllegalStateException e) {
								throw createJSonException(POTION_EFFECT_NAME, POTION_EFFECTS_NAME, jsonElement);
							}
						} else if(jsonObject.has(POTION_EFFECT_NAME)) {
							JsonElement jsonElement = jsonObject.get(POTION_EFFECT_NAME);
							try {
								IPotionEffect effect = effectAdapter.fromJsonTree(jsonElement);
								if(effect == null) {
									throw createJSonException(POTION_EFFECT_NAME, POTION_EFFECT_NAME, jsonElement);
								}
								
								item.addEffect(effect);
							} catch(ClassCastException | IllegalStateException e) {
								throw createJSonException(POTION_EFFECT_NAME, POTION_EFFECT_NAME, jsonElement);
							}
						}
					} else if(jsonObject.has(ITEM_ENCHANTMENTS_NAME) || jsonObject.has(ITEM_ENCHANTMENT_NAME)
							|| jsonObject.has(ITEM_NAME_NAME)) {
						item.setEnchanted(true);
						
						if(jsonObject.has(ITEM_NAME_NAME)) {
							JsonElement jsonElement = jsonObject.get(ITEM_NAME_NAME);
							try {
								String name = jsonElement.getAsString();
								if(name == null || name.isEmpty()) {
									throw createJSonException(ITEM_NAME_NAME, ITEM_NAME_NAME, jsonElement);
								}
								item.setItemName(name);
							} catch(ClassCastException | IllegalStateException e) {
								throw createJSonException(POTION_EFFECT_NAME, POTION_EFFECTS_NAME, jsonElement);
							}
						}
						
						if(jsonObject.has(ITEM_ENCHANTMENTS_NAME)) {
							item.setEnchanted(true);
							
							JsonElement jsonElement = jsonObject.get(ITEM_ENCHANTMENTS_NAME);
							try {
								
								JsonArray jsonArray = jsonElement.getAsJsonArray();
								for(JsonElement arrayElement : jsonArray) {
									EnchantmentConf enchantment = enchantmentAdapter.fromJsonTree(arrayElement);
									if(enchantment == null) {
										throw createJSonException(ITEM_ENCHANTMENT_NAME, ITEM_ENCHANTMENTS_NAME, arrayElement);
									}
									
									item.addEnchantment(enchantment);
								}
							} catch(ClassCastException | IllegalStateException e) {
								throw createJSonException(ITEM_ENCHANTMENT_NAME, ITEM_ENCHANTMENTS_NAME, jsonElement);
							}
						} else if(jsonObject.has(ITEM_ENCHANTMENT_NAME)) {
							JsonElement jsonElement = jsonObject.get(ITEM_ENCHANTMENT_NAME);
							
							EnchantmentConf enchantment = enchantmentAdapter.fromJsonTree(jsonElement);
							if(enchantment == null) {
								throw createJSonException(ITEM_ENCHANTMENT_NAME, ITEM_ENCHANTMENTS_NAME, jsonElement);
							}
							
							item.addEnchantment(enchantment);
						}
					}
					
					if(jsonObject.has(ITEM_ID_NAME)) {
						JsonElement jsonElement = jsonObject.get(ITEM_ID_NAME);
						try {
							IMaterial material = materialFactory.get(jsonElement.getAsString());
							if(material == null) {
								throw createJSonException(ITEM_ID_NAME, ITEM_ID_NAME, jsonElement);
							}
							item.setId(material);
						} catch(ClassCastException | IllegalStateException e) {
							throw createJSonException(ITEM_ID_NAME, ITEM_ID_NAME, jsonElement);
						}
					}
					
					if(jsonObject.has(ITEM_SLOT_NAME)) {
						JsonElement jsonElement = jsonObject.get(ITEM_SLOT_NAME);
						try {
							ISlot slot = slotFactory.get(jsonElement.getAsString());
							if(slot == null) {
								throw createJSonException(ITEM_SLOT_NAME, ITEM_SLOT_NAME, jsonElement);
							}
							item.setSlot(slot);
						} catch(ClassCastException | IllegalStateException e) {
							throw createJSonException(ITEM_SLOT_NAME, ITEM_SLOT_NAME, jsonElement);
						}
					}
					
					if(jsonObject.has(ITEM_AMOUNT_NAME)) {
						JsonElement jsonElement = jsonObject.get(ITEM_AMOUNT_NAME);
						try {
							IAmount amount = amountFactory.get(jsonElement.getAsString());
							if(amount == null) {
								throw createJSonException(ITEM_AMOUNT_NAME, ITEM_AMOUNT_NAME, jsonElement);
							}
							item.setAmount(amount);
						} catch(ClassCastException | IllegalStateException e) {
							throw createJSonException(ITEM_AMOUNT_NAME, ITEM_AMOUNT_NAME, jsonElement);
						}
					}
					
					if(jsonObject.has(ITEM_DAMAGE_NAME)) {
						System.out.println("damage!");
						JsonElement jsonElement = jsonObject.get(ITEM_DAMAGE_NAME);
						try {
							int damage = jsonElement.getAsInt();
							System.out.println("value: " + damage);
							if(damage < 0 || damage > Short.MAX_VALUE) {
								throw createJSonException(ITEM_DAMAGE_NAME, ITEM_DAMAGE_NAME, jsonElement);
							}
							item.setDamage(damage);
						} catch(ClassCastException | IllegalStateException e) {
							throw createJSonException(ITEM_DAMAGE_NAME, ITEM_DAMAGE_NAME, jsonElement);
						}
					}
					
					return item;
				} catch(ClassCastException | IllegalStateException e) {
					throw createJSonException(KIT_ITEM_NAME, KIT_ITEMS_NAME, element);
				}
			}
		};
	}
}