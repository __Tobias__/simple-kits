package org.tpc.minecraft.simplekits.conf.adapter;

import static org.tpc.minecraft.simplekits.Constants.DataModel.*;
import static org.tpc.minecraft.simplekits.conf.adapter.KitTypeAdapter.*;

import java.io.IOException;

import org.tpc.minecraft.simplekits.conf.impl.BaseReferenceConf;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.internal.Streams;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class BaseTypeAdapterFactory extends TypeAdapter<BaseReferenceConf> implements TypeAdapterFactory {
	
	private TypeAdapter<BaseReferenceConf> delegate;
	
	public BaseTypeAdapterFactory() { }
	
	public void setGson(Gson gson) {
		delegate = gson.getDelegateAdapter(this, TypeToken.get(BaseReferenceConf.class));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
		return BaseReferenceConf.class.isAssignableFrom(type.getRawType())
				? (TypeAdapter<T>)this : null;
	}
	
	@Override
	public void write(JsonWriter out, BaseReferenceConf value) throws IOException {
		if(!value.isBasic()) {
			delegate.write(out, value);
			return;
		}
		
		Streams.write(new JsonPrimitive(value.getName()), out);
	}
	
	@Override
	public BaseReferenceConf read(JsonReader in) throws IOException {
		JsonElement element = Streams.parse(in);
		if(element.isJsonPrimitive()) {
			try {
				String name = element.getAsString();
				if(name.startsWith(BASE_TYPE_KIT_NAME + ":")) {
					return BaseReferenceConf.kit(name.substring(BASE_TYPE_KIT_NAME.length() + 1));
				} else if(name.startsWith(BASE_TYPE_TEMPLATE_NAME + ":")) {
					return BaseReferenceConf.template(name.substring(BASE_TYPE_TEMPLATE_NAME.length() + 1));
				} else {
					for(String refName : BASE_TYPE_NAMES) {
						if(name.startsWith(refName + ":")) {
							return BaseReferenceConf.reference(name.substring(refName.length() + 1));
						}
					}
				}
				
				return BaseReferenceConf.reference(name);
			} catch(IllegalArgumentException | ClassCastException e) {
				throw createJSonException(KIT_BASE_NAME, KIT_BASES_NAME, element);
			}
		} else {
			JsonObject childJsonObject = element.getAsJsonObject();
			BaseReferenceConf referenceConf = null;
			
			if(childJsonObject.has(BASE_TYPE_KIT_NAME)) {
				try {
					referenceConf = BaseReferenceConf.kit(childJsonObject.get(BASE_TYPE_KIT_NAME).getAsString());
				} catch(ClassCastException | IllegalStateException e) {
					throw createJSonException(KIT_BASE_NAME, KIT_BASES_NAME, childJsonObject);
				}
			}
			if(childJsonObject.has(BASE_TYPE_TEMPLATE_NAME)) {
				try {
					referenceConf = BaseReferenceConf.template(childJsonObject.get(BASE_TYPE_TEMPLATE_NAME).getAsString());
				} catch(ClassCastException | IllegalStateException e) {
					throw createJSonException(KIT_BASE_NAME, KIT_BASES_NAME, childJsonObject);
				}
			}
			
			for(String name : BASE_TYPE_NAMES) {
				if(childJsonObject.has(name)) {
					try {
						referenceConf = BaseReferenceConf.reference(childJsonObject.get(name).getAsString());
					} catch(ClassCastException | IllegalStateException e) {
						throw createJSonException(KIT_BASE_NAME, KIT_BASES_NAME, childJsonObject);
					}
				}
			}
			
			if(referenceConf == null) {
				throw createJSonException(KIT_BASE_NAME, KIT_BASES_NAME, element);
			}
			
			if(childJsonObject.has(BASE_PRIORITY_NAME)) {
				try {
					referenceConf.setPriority(childJsonObject.get(BASE_PRIORITY_NAME).getAsLong());
				} catch(ClassCastException | IllegalStateException e) {
					throw createJSonException(KIT_BASE_NAME, KIT_BASES_NAME, childJsonObject);
				}
			}
			
			return referenceConf;
		}
	}
}