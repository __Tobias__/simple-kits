package org.tpc.minecraft.simplekits.conf.impl;

import static org.tpc.minecraft.simplekits.Constants.DataModel.*;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KitConf {
	
	@Expose
	@SerializedName(KIT_ALIASES_NAME)
	private List<String> aliases;
	
	@Expose
	@SerializedName(KIT_BASES_NAME)
	private List<BaseReferenceConf> bases;
	
	@Expose
	@SerializedName(KIT_ITEMS_NAME)
	private List<ComponentConf> items;
	
	@Expose
	@SerializedName(KIT_COOLDOWN_NAME)
	private long cooldown;
	
	@Expose(deserialize = false, serialize = false)
	private Path source;
	
	public KitConf() {
		items = new ArrayList<>();
		bases = new ArrayList<>();
		aliases = new ArrayList<>();
	}
	
	public List<String> getAliases() {
		return aliases;
	}
	
	public void addAlias(String alias) {
		aliases.add(alias);
	}
	
	public void removeAlias(String alias) {
		aliases.remove(alias);
	}
	
	public void setAliases(List<String> aliases) {
		this.aliases = aliases;
	}
	
	public List<BaseReferenceConf> getBases() {
		return bases;
	}
	
	public void addBase(BaseReferenceConf base) {
		bases.add(base);
	}
	
	public void removeBase(BaseReferenceConf base) {
		bases.remove(base);
	}
	
	public void setBases(List<BaseReferenceConf> base) {
		this.bases = base;
	}
	
	public List<ComponentConf> getItems() {
		return items;
	}
	
	public void addItem(ComponentConf item) {
		items.add(item);
	}
	
	public void removeItem(ComponentConf item) {
		items.remove(item);
	}
	
	public void setItems(List<ComponentConf> items) {
		this.items = items;
	}
	
	public long getCooldown() {
		return cooldown;
	}
	
	public void setCooldown(long cooldown) {
		this.cooldown = cooldown;
	}
	
	public Path getSource() {
		return source;
	}
	
	public void setSource(Path source) {
		this.source = source;
	}
}