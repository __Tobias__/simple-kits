package org.tpc.minecraft.simplekits.conf.impl;

import static org.tpc.minecraft.simplekits.Constants.DataModel.*;

import org.bukkit.enchantments.Enchantment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EnchantmentConf {
	
	@Expose
	@SerializedName(ENCHANTMENT_TYPE_NAME)
	private Enchantment type;
	
	@Expose
	@SerializedName(ENCHANTMENT_LEVEL_NAME)
	private long level;
	
	@Expose(deserialize = false, serialize = false)
	private String name;
	
	public EnchantmentConf() {
		level = 1;
	}
	
	public long getLevel() {
		return level;
	}
	
	public void setLevel(long level) {
		this.level = level;
	}
	
	public Enchantment getType() {
		return type;
	}
	
	public void setType(Enchantment type) {
		this.type = type;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}