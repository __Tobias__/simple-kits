package org.tpc.minecraft.simplekits.conf.adapter.bukkit;

import java.io.IOException;

import org.bukkit.enchantments.Enchantment;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class BukkitEnchantmentTypeAdapter extends TypeAdapter<Enchantment> {
	
	@Override
	public void write(JsonWriter out, Enchantment value) throws IOException {
		Streams.write(new JsonPrimitive(value.getName()), out);
	}
	
	@Override
	public Enchantment read(JsonReader in) throws IOException {
		JsonElement element = Streams.parse(in);
		
		try {
			return Enchantment.getByName(element.getAsString());
		} catch(IllegalArgumentException | ClassCastException e) {
			return null;
		}
	}
}