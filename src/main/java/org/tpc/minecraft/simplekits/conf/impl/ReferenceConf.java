package org.tpc.minecraft.simplekits.conf.impl;

import static org.tpc.minecraft.simplekits.Constants.DataModel.*;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReferenceConf extends ComponentConf {
	
	public static enum Type {
		Reference, Kit, Template
	}
	
	public static ReferenceConf reference(String name) {
		return new ReferenceConf(name, Type.Reference);
	}
	
	public static ReferenceConf kit(String name) {
		return new ReferenceConf(name, Type.Kit);
	}
	
	public static ReferenceConf template(String name) {
		return new ReferenceConf(name, Type.Template);
	}
	
	@Expose
	@SerializedName(REFERENCE_NAME)
	private String name;
	
	@Expose
	@SerializedName(REFERENCE_TYPE_NAME)
	private Type type;
	
	protected ReferenceConf(String name, Type type) {
		this.name = name;
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
}