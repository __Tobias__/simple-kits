package org.tpc.minecraft.simplekits;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.PersistenceException;

import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.tpc.minecraft.simplekits.commands.AdminCommands;
import org.tpc.minecraft.simplekits.commands.KitCommands;
import org.tpc.minecraft.simplekits.commands.KitsCommands;
import org.tpc.minecraft.simplekits.commands.kit.ListCommand;
import org.tpc.minecraft.simplekits.data.Cooldown;
import org.tpc.minecraft.simplekits.data.Ignore;
import org.tpc.minecraft.simplekits.impl.DefaultKitsFactory;
import org.tpc.minecraft.simplekits.impl.KitCooldowns;
import org.tpc.minecraft.simplekits.impl.KitIgnores;
import org.tpc.minecraft.simplekits.impl.KitPlacer;
import org.tpc.minecraft.simplekits.kit.IKit;

import static org.tpc.minecraft.simplekits.Constants.GlobalMessages.*;

public class SimpleKits extends JavaPlugin {
	
	private PluginManager pluginManager;
	
	private IKitsFactory kitsFactory;
	
	private Configuration configuration;
	private IKits kits;
	private IKitPlacer kitPlacer;
	private ICooldowns cooldowns;
	private IIgnores ignores;
	
	private boolean initialized;
	
	public SimpleKits() {
		configuration = new Configuration(this);
		kitsFactory = new DefaultKitsFactory(this);
		cooldowns = new KitCooldowns(this);
		ignores = new KitIgnores(this);
		kitPlacer = new KitPlacer(this);
		
		initialized = false;
	}
	
	@Override
	public void onLoad() {
		pluginManager = getServer().getPluginManager();
		
		saveDefaultConfig();
		configuration.load();
		kits = kitsFactory.load();
	}
	
	private void registerPermissions() {
		for(Permission permission : Constants.Permissions.PERMISSIONS) {
			registerPermissions(permission);
		}
	}
	
	private void registerPermissions(Permission permission) {
		PermissionDefault defaultValue = permission.getDefault();
		
		for(IKit kit : kits.getKits()){
			pluginManager.addPermission(new Permission(permission.getName() + "." + kit.getName(), defaultValue));
		}
		
		for(String kitAlias : kits.getAliasMap().getAliases()) {
			pluginManager.addPermission(new Permission(permission.getName() + "." + kitAlias, defaultValue));
		}
	}
	
	private void registerCommands() {
		ListCommand listCommand = new ListCommand(this);
		KitCommands kitCommands = new KitCommands(this, listCommand);
		
		getCommand("kit").setExecutor(kitCommands);
		getCommand("kits").setExecutor(new KitsCommands(listCommand));
		getCommand("kitadmin").setExecutor(new AdminCommands(this));
		
		kitCommands.onLoad();
	}
	
	public String getMessage(String path, Object ... parameter) {
		return configuration.getMessage(path, parameter);
	}
	
	@Override
	public void onEnable() {
		if(!initialized) {
			initialized = true;
			
			try {
				// Make sure all tables exist
				for(Class<?> clazz : getDatabaseClasses()) {
					getDatabase().find(clazz).findIterate().close();
				}
			} catch(PersistenceException ignoredPersistenceException) {
				getLogger().log(Level.WARNING, GlobalMessages.getString(GMESSAGE_DB_ERROR_NOT_FOUND));
				
				try {
					removeDDL(); // Cleanup
				} catch (PersistenceException persistenceException) { }
				
				try {
					installDDL();
				} catch (PersistenceException persistenceException) {
					getLogger().log(Level.WARNING, GlobalMessages.getString(GMESSAGE_DB_ERROR_INIT_FAILED), persistenceException);
				}
			}
			
			registerPermissions();
			registerCommands();
		}
		
		cooldowns.onLoad();
		ignores.onLoad();
	}
	
	@Override
	public void onDisable() {
		cooldowns.onUnload();
		ignores.onUnload();
	}
	
	public PluginManager getPluginManager() {
		return pluginManager;
	}
	
	public Configuration getConfiguration() {
		return configuration;
	}
	
	@Override
	public List<Class<?>> getDatabaseClasses() {
		return Arrays.asList(Cooldown.class, Ignore.class);
	}
	
	public IKits getKits() {
		return kits;
	}
	
	public IKitPlacer getKitPlacer() {
		return kitPlacer;
	}
	
	public ICooldowns getCooldowns() {
		return cooldowns;
	}
	
	public IIgnores getIgnores() {
		return ignores;
	}
}