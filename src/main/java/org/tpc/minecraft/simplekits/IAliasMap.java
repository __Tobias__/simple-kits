package org.tpc.minecraft.simplekits;

public interface IAliasMap {
	
	public boolean aliasExists(String alias);
	public String getObjectForAlias(String alias);
	public Iterable<String> getAliases(String object);
	public Iterable<String> getAliases();
	
}