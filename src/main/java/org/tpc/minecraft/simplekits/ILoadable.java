package org.tpc.minecraft.simplekits;

public interface ILoadable {
	
	public void onLoad();
	public void onUnload();
	
}