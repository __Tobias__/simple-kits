package org.tpc.minecraft.simplekits.kit;

public interface ISlot {
	
	public String getName();
	public long getSlot();
	
}