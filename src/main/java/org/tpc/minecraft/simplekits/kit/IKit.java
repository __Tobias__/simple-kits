package org.tpc.minecraft.simplekits.kit;

import java.nio.file.Path;

public interface IKit extends IOverwrite, IDiscard {
	
	public Iterable<IBaseReference> getBases();
	public Iterable<IComponent> getComponents();
	public long getCooldown();
	public boolean isAlias();
	
	public String getName();
	public String getDescription();
	public Path getSource();
	
}