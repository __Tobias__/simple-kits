package org.tpc.minecraft.simplekits.kit;

public interface IEnchantableItem extends IComponent {
	
	public Iterable<IEnchantment> getEnchantments();
	public String getItemName();
	
	public default boolean isEnchanted() {
		return (getEnchantments() != null) || (getItemName() != null);
	}
}