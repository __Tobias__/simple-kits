package org.tpc.minecraft.simplekits.kit.impl;

import org.tpc.minecraft.simplekits.kit.IBaseReference;
import org.tpc.minecraft.simplekits.kit.IKit;

public class BaseReference implements IBaseReference {
	
	public static enum Type {
		Reference, Kit, Template
	}
	
	public static BaseReference reference(String name) {
		return new BaseReference(name, Type.Reference);
	}
	
	public static BaseReference kit(String name) {
		return new BaseReference(name, Type.Kit);
	}
	
	public static BaseReference template(String name) {
		return new BaseReference(name, Type.Template);
	}
	
	private String name;
	private IKit kit;
	private Type type;
	
	public BaseReference(String name, Type type) {
		this.name = name;
		this.type = type;
	}
	
	public BaseReference(IKit kit) {
		this.name = kit.getName();
		this.kit = kit;
	}
	
	public BaseReference(String name, IKit kit) {
		this.name = name;
		this.kit = kit;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public IKit getKit() {
		return kit;
	}
	
	public void setKit(IKit kit) {
		this.kit = kit;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Type getType() {
		return type;
	}
}