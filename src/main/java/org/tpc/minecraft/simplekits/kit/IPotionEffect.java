package org.tpc.minecraft.simplekits.kit;

import org.bukkit.potion.PotionEffectType;

public interface IPotionEffect {
	
	public PotionEffectType getType();
	public int getDuration();
	public int getAmplifier();
	
	public boolean isSplash();
	
}