package org.tpc.minecraft.simplekits.kit;

@FunctionalInterface
public interface IAbort {
	
	public boolean isAbortIfOccupied();
	
}