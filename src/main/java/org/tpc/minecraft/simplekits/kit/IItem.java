package org.tpc.minecraft.simplekits.kit;

public interface IItem extends IComponent {
	
	public IMaterial getMaterial();
	public IAmount getAmount();
	public ISlot getSlot();
	public int getDamage();
	public IItem createStack(long size);
	
	public boolean isPotion();
	public boolean isEnchantable();
	
}