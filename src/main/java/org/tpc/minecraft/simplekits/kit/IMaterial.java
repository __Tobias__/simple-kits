package org.tpc.minecraft.simplekits.kit;

import org.bukkit.Material;

public interface IMaterial extends Comparable<IMaterial> {
	
	public Material getMaterial();
	public boolean isEnchantable();
	public boolean isPotion();
	public String getName();
	public int getDamage();
	
}