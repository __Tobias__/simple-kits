package org.tpc.minecraft.simplekits.kit.impl;

import org.tpc.minecraft.simplekits.kit.IAbort;

public class Abort implements IAbort {
	
	private boolean abortIfOccupied;
	
	public void setAbortIfOccupied(boolean abortIfOccupied) {
		this.abortIfOccupied = abortIfOccupied;
	}
	
	@Override
	public boolean isAbortIfOccupied() {
		return abortIfOccupied;
	}
}