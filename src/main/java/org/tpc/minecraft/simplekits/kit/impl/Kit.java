package org.tpc.minecraft.simplekits.kit.impl;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.tpc.minecraft.simplekits.kit.IBaseReference;
import org.tpc.minecraft.simplekits.kit.IComponent;
import org.tpc.minecraft.simplekits.kit.IKit;

public class Kit extends OverwriteDiscardBase implements IKit {
	
	private long cooldown;
	private String name;
	private String description;
	
	private Collection<IComponent> components;
	private Collection<IBaseReference> baseReferences;
	
	private Path source;
	
	public Kit() {
		setDropOnFloor(false);
		setDiscard(false);
		setDiscardIfOccupied(false);
	}
	
	public Kit(long cooldown, String name) {
		this();
		this.components = new ArrayList<>();
		this.baseReferences = new ArrayList<>();
		
		this.cooldown = cooldown;
		this.name = name;
	}
	
	@Override
	public Iterable<IBaseReference> getBases() {
		return Collections.unmodifiableCollection(baseReferences);
	}
	
	@Override
	public Iterable<IComponent> getComponents() {
		return Collections.unmodifiableCollection(components);
	}
	
	public void addBase(IBaseReference baseReference) {
		baseReferences.add(baseReference);
	}
	
	public void addComponent(IComponent component) {
		components.add(component);
	}
	
	public void addComponents(Collection<IComponent> components) {
		components.addAll(components);
	}
	
	public void setCooldown(long cooldown) {
		this.cooldown = cooldown;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setSource(Path source) {
		this.source = source;
	}
	
	@Override
	public long getCooldown() {
		return cooldown;
	}
	
	@Override
	public boolean isAlias() {
		return false;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String getDescription() {
		return description;
	}
	
	@Override
	public Path getSource() {
		return source;
	}
}