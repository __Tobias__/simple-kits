package org.tpc.minecraft.simplekits.kit.impl;

import java.util.Objects;

import org.tpc.minecraft.simplekits.kit.IEnchantment;

public class Enchantment implements IEnchantment {
	
	private String name;
	private int level;
	private org.bukkit.enchantments.Enchantment enchantment;
	
	public Enchantment(org.bukkit.enchantments.Enchantment enchantment, String name) {
		this.enchantment = enchantment;
		this.name = name;
		level = 1;
	}
	
	public Enchantment(org.bukkit.enchantments.Enchantment enchantment) {
		this(enchantment, null);
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public org.bukkit.enchantments.Enchantment getEnchantment() {
		return enchantment;
	}
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof IEnchantment) {
			IEnchantment otherEnchantment = (IEnchantment) other;
			
			return Objects.equals(name, otherEnchantment.getName())
					&& Objects.equals(enchantment, otherEnchantment.getEnchantment());
		}
		
		return false;
	}
	
	@Override
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setEnchantment(org.bukkit.enchantments.Enchantment enchantment) {
		this.enchantment = enchantment;
	}
}