package org.tpc.minecraft.simplekits.kit;

public interface IOverwrite {
	
	public boolean isOverwrite();
	public boolean isForceOverwrite();
	
	public IOverwrite merge(IOverwrite other);
	
}