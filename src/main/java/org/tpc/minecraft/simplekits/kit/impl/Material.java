package org.tpc.minecraft.simplekits.kit.impl;

import org.tpc.minecraft.simplekits.kit.IMaterial;

public class Material implements IMaterial {
	
	private String name;
	private boolean enchantable;
	private org.bukkit.Material material;
	private int damage;
	
	public Material(org.bukkit.Material material, boolean enchantable, String name, int damage) {
		this.material = material;
		this.enchantable = enchantable;
		this.name = name;
		this.damage = damage;
	}
	
	@Override
	public org.bukkit.Material getMaterial() {
		return material;
	}
	
	@Override
	public boolean isEnchantable() {
		return enchantable;
	}
	
	@Override
	public boolean isPotion() {
		return material == org.bukkit.Material.POTION;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public int compareTo(IMaterial other) {
		if(isPotion() != other.isPotion()) {
			return isPotion() ? 1 : -1;
		}
		
		if(isEnchantable() != other.isEnchantable()) {
			return isEnchantable() ? 1 : -1;
		}
		
		return getMaterial().compareTo(other.getMaterial());
	}
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof IMaterial) {
			return (compareTo((IMaterial) other) == 0);
		}
		
		return false;
	}
	
	@Override
	public int getDamage() {
		return damage;
	}
}