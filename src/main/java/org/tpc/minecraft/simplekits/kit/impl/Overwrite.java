package org.tpc.minecraft.simplekits.kit.impl;

import org.tpc.minecraft.simplekits.kit.IOverwrite;

public class Overwrite implements IOverwrite {
	
	private boolean overwrite;
	private boolean forceOverwrite;
	
	@Override
	public boolean isOverwrite() {
		return overwrite;
	}
	
	@Override
	public boolean isForceOverwrite() {
		return forceOverwrite;
	}
	
	public void setForceOverwrite(boolean forceOverwrite) {
		this.forceOverwrite = forceOverwrite;
	}
	
	public void setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
	}
	
	@Override
	public IOverwrite merge(IOverwrite other) {
		Overwrite ret = new Overwrite();
		
		ret.setOverwrite(overwrite || other.isOverwrite());
		ret.setForceOverwrite(forceOverwrite || other.isForceOverwrite());
		
		return ret;
	}
	
}