package org.tpc.minecraft.simplekits.kit.impl;

import static org.tpc.minecraft.simplekits.Constants.Defaults.*;

import org.tpc.minecraft.simplekits.kit.IKit;
import org.tpc.minecraft.simplekits.kit.IReference;

public class Reference extends OverwriteDiscardBase implements IReference {
	
	public static enum Type {
		Reference, Kit, Template
	}
	
	public static Reference reference(String name) {
		return new Reference(name, Type.Reference);
	}
	
	public static Reference kit(String name) {
		return new Reference(name, Type.Kit);
	}
	
	public static Reference template(String name) {
		return new Reference(name, Type.Template);
	}
	
	private String name;
	private IKit kit;
	private boolean ignoreSlots;
	private Type type;
	
	private Reference() {
		setIgnoreSlots(REFERENCE_IGNORE_SLOTS);
	}
	
	public Reference(String name, Type type) {
		this();
		this.name = name;
		this.type = type;
	}
	
	public Reference(String name, IKit kit) {
		this();
		this.name = name;
		this.kit = kit;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	
	@Override
	public IKit getKit() {
		return kit;
	}
	
	@Override
	public boolean isIgnoringSlots() {
		return ignoreSlots;
	}
	
	public void setIgnoreSlots(boolean ignoreSlots) {
		this.ignoreSlots = ignoreSlots;
	}
	
	public void setKit(IKit kit) {
		this.kit = kit;
	}
	
	public Type getType() {
		return type;
	}
}