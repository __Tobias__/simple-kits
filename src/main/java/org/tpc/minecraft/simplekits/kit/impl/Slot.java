package org.tpc.minecraft.simplekits.kit.impl;

import org.tpc.minecraft.simplekits.kit.ISlot;

public class Slot implements ISlot {
	
	private String name;
	private long slot;
	
	public Slot(long slot, String name) {
		this.slot = slot;
		this.name = name;
	}
	
	public Slot(long slot) {
		this.slot = slot;
		this.name = null;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public long getSlot() {
		return slot;
	}
}