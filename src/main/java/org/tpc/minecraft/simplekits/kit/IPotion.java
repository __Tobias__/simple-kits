package org.tpc.minecraft.simplekits.kit;

import java.util.List;

public interface IPotion extends IComponent {
	
	public List<IPotionEffect> getEffects();
	
	public default boolean hasEffect() {
		return !getEffects().isEmpty();
	}
}