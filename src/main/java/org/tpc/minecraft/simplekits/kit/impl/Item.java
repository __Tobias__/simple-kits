package org.tpc.minecraft.simplekits.kit.impl;

import java.util.ArrayList;
import java.util.List;

import org.tpc.minecraft.simplekits.def.IAmountFactory;
import org.tpc.minecraft.simplekits.kit.IAmount;
import org.tpc.minecraft.simplekits.kit.IEnchantableItem;
import org.tpc.minecraft.simplekits.kit.IEnchantment;
import org.tpc.minecraft.simplekits.kit.IItem;
import org.tpc.minecraft.simplekits.kit.IMaterial;
import org.tpc.minecraft.simplekits.kit.IPotion;
import org.tpc.minecraft.simplekits.kit.IPotionEffect;
import org.tpc.minecraft.simplekits.kit.ISlot;

public class Item implements IItem, IPotion, IEnchantableItem {
	
	private IAmountFactory amountFactory;
	
	private IMaterial material;
	private IAmount amount;
	private ISlot slot;
	private int damage;
	
	private boolean potion;
	private List<IPotionEffect> potionEffects;
	
	public boolean enchantable;
	private String itemName;
	private List<IEnchantment> enchantments;
	
	public Item(IAmountFactory amountFactory, IMaterial material, IAmount amount, ISlot slot) {
		this.amountFactory = amountFactory;
		this.material = material;
		this.amount = (amount != null) ? amount : amountFactory.getDefault(material);
		this.slot = slot;
		
		this.potion = false;
		this.potionEffects = new ArrayList<>();
		
		this.enchantable = false;
		this.itemName = null;
		this.enchantments = new ArrayList<>();
		
		this.damage = -1;
	}
	
	@Override
	public IMaterial getMaterial() {
		return material;
	}
	
	@Override
	public IAmount getAmount() {
		return amount;
	}
	
	@Override
	public ISlot getSlot() {
		return slot;
	}
	
	@Override
	public IItem createStack(long size) {
		Item item = new Item(amountFactory, material, amountFactory.get(size), null);
		
		if(isPotion()) {
			item.setPotionEffects(getEffects());
		}
		
		return item;
	}
	
	@Override
	public boolean isPotion() {
		return potion;
	}
	
	public void setPotion(boolean potion) {
		this.potion = potion;
	}
	
	@Override
	public boolean isEnchantable() {
		return enchantable;
	}
	
	public void addPotionEffect(IPotionEffect effect) {
		potionEffects.add(effect);
	}
	
	public void removePotionEffect(IPotionEffect effect) {
		potionEffects.remove(effect);
	}
	
	public void clearPotionEffect() {
		potionEffects.clear();
	}
	
	public void setPotionEffects(List<IPotionEffect> effects) {
		this.potionEffects = effects;
	}
	
	@Override
	public List<IPotionEffect> getEffects() {
		return potionEffects;
	}
	
	public void setEnchanted(boolean enchantable) {
		this.enchantable = enchantable;
	}
	
	public void setEnchantments(List<IEnchantment> enchantments) {
		this.enchantments = enchantments;
	}
	
	public void addEnchantment(IEnchantment enchantment) {
		enchantments.add(enchantment);
	}
	
	public void removeEnchantment(IEnchantment enchantment) {
		enchantments.remove(enchantment);
	}
	
	public void clearEnchantments() {
		enchantments.clear();
	}
	
	@Override
	public Iterable<IEnchantment> getEnchantments() {
		return enchantments;
	}
	
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	@Override
	public String getItemName() {
		return itemName;
	}
	
	@Override
	public int getDamage() {
		return damage;
	}
	
	public void setDamage(int damage) {
		this.damage = damage;
	}
}