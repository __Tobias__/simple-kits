package org.tpc.minecraft.simplekits.kit;

import org.bukkit.enchantments.Enchantment;

public interface IEnchantment {
	
	public Enchantment getEnchantment();
	public int getLevel();
	public String getName();
	
}