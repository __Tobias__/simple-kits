package org.tpc.minecraft.simplekits.kit.impl;

import org.tpc.minecraft.simplekits.kit.IAmount;
import org.tpc.minecraft.simplekits.kit.IMaterial;

public class Amount implements IAmount {
	
	private String name;
	private long amount;
	
	public Amount(long amount, String name) {
		this.amount = amount;
		this.name = name;
	}
	
	public Amount(long amount) {
		this.amount = amount;
		this.name = null;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public long getValue(IMaterial material) {
		return amount;
	}
}