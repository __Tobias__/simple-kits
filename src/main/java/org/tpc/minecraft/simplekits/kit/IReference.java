package org.tpc.minecraft.simplekits.kit;

public interface IReference extends IComponent, IOverwrite, IDiscard {
	
	public String getName();
	public IKit getKit();
	public boolean isIgnoringSlots();
	
}