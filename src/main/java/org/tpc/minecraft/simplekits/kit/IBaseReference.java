package org.tpc.minecraft.simplekits.kit;

public interface IBaseReference {
	
	public String getName();
	public IKit getKit();
	
}