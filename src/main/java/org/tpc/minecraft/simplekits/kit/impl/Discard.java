package org.tpc.minecraft.simplekits.kit.impl;

import org.tpc.minecraft.simplekits.kit.IDiscard;

public class Discard implements IDiscard {
	
	private boolean discard;
	private boolean discardIfOccupied;
	private boolean dropOnFloor;
	
	@Override
	public boolean isDiscard() {
		return discard;
	}
	
	@Override
	public boolean isDiscardIfOccupied() {
		return discardIfOccupied;
	}
	
	public void setDiscard(boolean discard) {
		this.discard = discard;
	}
	
	public void setDiscardIfOccupied(boolean discardIfOccupied) {
		this.discardIfOccupied = discardIfOccupied;
	}
	
	@Override
	public boolean isDropOnFloor() {
		return dropOnFloor;
	}
	
	public void setDropOnFloor(boolean dropOnFloor) {
		this.dropOnFloor = dropOnFloor;
	}
	
	@Override
	public IDiscard merge(IDiscard other) {
		Discard ret = new Discard();
		
		ret.setDiscard(discard && other.isDiscard());
		ret.setDiscardIfOccupied(discardIfOccupied && other.isDiscardIfOccupied());
		
		return ret;
	}
}