package org.tpc.minecraft.simplekits.kit;

public interface IDiscard {
	
	public boolean isDiscard();
	public boolean isDiscardIfOccupied();
	public boolean isDropOnFloor();
	
	public IDiscard merge(IDiscard other);
	
}