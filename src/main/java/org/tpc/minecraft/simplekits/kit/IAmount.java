package org.tpc.minecraft.simplekits.kit;

public interface IAmount {
	
	public String getName();
	public long getValue(IMaterial material);
	
}