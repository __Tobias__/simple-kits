package org.tpc.minecraft.simplekits;

import static org.tpc.minecraft.simplekits.Constants.Configuration.*;
import static org.tpc.minecraft.simplekits.Constants.GlobalMessages.*;

import java.nio.file.DirectoryStream.Filter;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import net.time4j.PrettyTime;

public class Configuration {
	
	private SimpleKits plugin;
	private FileConfiguration config;
	
	private Locale locale;
	private PrettyTime periodFormatter;
	
	private Path basePath;
	private Path configPath;
	private List<Path> kitPaths;
	private List<Path> templatePaths;
	private List<PathMatcher> ignores;
	
	private String namePrefix;
	private String nameSuffix;
	private String nameTemplatePrefix;
	private String nameTemplateSuffix;
	
	private DefaultResources defaultResources;
	
	private Messages messages;
	
	private Map<String, ConfigurationSection> features;
	
	public Configuration(SimpleKits plugin) {
		this.plugin = plugin;
		
		kitPaths = new ArrayList<>();
		templatePaths = new ArrayList<>();
		ignores = new ArrayList<>();
		
		defaultResources = new DefaultResources(plugin);
		
		messages = new Messages(plugin);
		
		features = new HashMap<>();
	}
	
	public void load() {
		basePath = plugin.getDataFolder().toPath();
		getLogger().log(Level.INFO, GlobalMessages.getString(
				GMESSAGE_CONFIG_INFO_PLUGIN_FOLDER, basePath.toString()));
		
		config = plugin.getConfig();
		
		try {
			configPath = Paths.get(config.getString(CONFIG_PATHS_KEY));
			if(!configPath.isAbsolute()) {
				configPath = basePath.resolve(configPath);
			}
		} catch(InvalidPathException | NullPointerException e) {
			getLogger().log(Level.WARNING, GlobalMessages.getString(
					GMESSAGE_CONFIG_ERROR_INVALID_CONFIG_PATH, config.getString(CONFIG_PATHS_KEY)), e);
			configPath = basePath;
		}
		
		defaultResources.extract(configPath);
		
		List<String> kitsPaths = config.getStringList(CONFIG_KITS_PATHS_KEY);
		if(kitsPaths != null) {
			for(String pathString : kitsPaths) {
				try {
					Path parsedPath = Paths.get(pathString);
					if(!parsedPath.isAbsolute()) {
						parsedPath = configPath.resolve(parsedPath);
					}
					
					this.kitPaths.add(parsedPath);
					getLogger().log(Level.INFO, GlobalMessages.getString(
							GMESSAGE_CONFIG_INFO_KIT_PATH_ADDED, parsedPath));
				} catch(InvalidPathException e) {
					getLogger().log(Level.WARNING, GlobalMessages.getString(
							GMESSAGE_CONFIG_ERROR_KIT_PATH_IGNORED, pathString), e);
				}
			}
		} else {
			getLogger().log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_NO_KIT_PATHS_DEFINED));
		}
		
		List<String> templatesPaths = config.getStringList(CONFIG_TEMPLATES_PATHS_KEY);
		if(templatesPaths != null) {
			for(String pathString : templatesPaths) {
				try {
					Path parsedPath = Paths.get(pathString);
					if(!parsedPath.isAbsolute()) {
						parsedPath = configPath.resolve(parsedPath);
					}
					
					this.templatePaths.add(parsedPath);
					getLogger().log(Level.INFO, GlobalMessages.getString(
							GMESSAGE_CONFIG_INFO_TEMPLATE_PATH_ADDED, parsedPath));
				} catch(InvalidPathException e) {
					getLogger().log(Level.WARNING, GlobalMessages.getString(
							GMESSAGE_CONFIG_ERROR_TEMPLATE_PATH_IGNORED, pathString), e);
				}
			}
		} else {
			getLogger().log(Level.INFO, GlobalMessages.getString(
					GMESSAGE_CONFIG_ERROR_NO_TEMPLATE_PATHS_DEFINED));
		}
		
		List<String> ignores = config.getStringList(CONFIG_IGNORES_KEY);
		if(ignores != null) {
			FileSystem fileSystem = FileSystems.getDefault();
			
			for(String ignoreExpression : ignores) {
				try {
					this.ignores.add(fileSystem.getPathMatcher("glob:" + ignoreExpression)); //$NON-NLS-1$
					getLogger().log(Level.INFO, GlobalMessages.getString(GMESSAGE_CONFIG_INFO_IGNORE_ADDED,
							ignoreExpression));
				} catch(IllegalArgumentException | UnsupportedOperationException e) {
					getLogger().log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_IGNORE_INVALID,
							ignoreExpression), e);
				}
			}
		}
		
		namePrefix = stripPathPrefix(config.getString(CONFIG_NAME_KIT_PREFIX_KEY),
				CONFIG_NAME_KIT_PREFIX_KEY).toLowerCase();
		nameSuffix = stripPathSuffix(config.getString(CONFIG_NAME_KIT_SUFFIX_KEY),
				CONFIG_NAME_KIT_SUFFIX_KEY).toLowerCase();
		nameTemplatePrefix = stripPathPrefix(config.getString(CONFIG_NAME_TEMPLATE_PREFIX_KEY),
				CONFIG_NAME_TEMPLATE_PREFIX_KEY).toLowerCase();
		nameTemplateSuffix = stripPathSuffix(config.getString(CONFIG_NAME_TEMPLATE_SUFFIX_KEY),
				CONFIG_NAME_TEMPLATE_SUFFIX_KEY).toLowerCase();
		
		locale = Locale.forLanguageTag(config.getString(CONFIG_KEY_LOCALE, CONFIG_DEFAULT_LOCALE));
		periodFormatter = PrettyTime.of(locale);
		
		Path messagesPath;
		try {
			messagesPath = Paths.get(config.getString(CONFIG_KEY_MESSAGES));
			if(!messagesPath.isAbsolute()) {
				messagesPath = basePath.resolve(messagesPath);
			}
		} catch(InvalidPathException | NullPointerException e) {
			getLogger().log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_INVALID_MESSAGES,
					config.getString(CONFIG_KEY_MESSAGES)), e);
			messagesPath = basePath.resolve(CONFIG_DEFAULT_MESSAGES);
		}
		
		messages.load(messagesPath, locale);
		
		ConfigurationSection section = config.getConfigurationSection("features");
		if(section != null) {
			for(String key : section.getKeys(false)) {
				features.put(key.toLowerCase(), section.getConfigurationSection(key));
			}
		}
	}
	
	private Logger getLogger() {
		return plugin.getLogger();
	}
	
	public Locale getLocale() {
		return locale;
	}
	
	public PrettyTime getPeriodFormatter() {
		return periodFormatter;
	}
	
	private final String stripPathPrefix(String text, String name) {
		if(text.contains("\\") || text.contains("/")) { //$NON-NLS-1$ //$NON-NLS-2$
			getLogger().log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_NO_PATH_SEPARATOR_IN_PREFIX
					, name, CONFIG_PATHS_KEY, CONFIG_KITS_PATHS_KEY, CONFIG_TEMPLATES_PATHS_KEY));
			return text.substring(Math.min(text.lastIndexOf('\\'), text.lastIndexOf('/')));
		}
		
		return text;
	}
	
	private final String stripPathSuffix(String text, String name) {
		if(text.contains("\\") || text.contains("/")) { //$NON-NLS-1$ //$NON-NLS-2$
			getLogger().log(Level.WARNING, GlobalMessages.getString(GMESSAGE_CONFIG_ERROR_NO_PATH_SEPARATOR_IN_SUFFIX, name));
			return text.substring(0, Math.min(text.lastIndexOf('\\'), text.lastIndexOf('/')));
		}
		
		return text;
	}
	
	public Path getBasePath() {
		return basePath;
	}
	
	public List<Path> getKitPaths() {
		return kitPaths;
	}
	
	public List<Path> getTemplatePaths() {
		return templatePaths;
	}
	
	public Filter<Path> getTemplateFilter() {
		return (_path) -> {
			String name = _path.getFileName().toString().toLowerCase();
			return (name.startsWith(nameTemplatePrefix) && name.endsWith(nameTemplateSuffix));
		};
	}
	
	public Filter<Path> getKitFilter() {
		return (_path) -> {
			String name = _path.getFileName().toString().toLowerCase();
			return (name.startsWith(namePrefix) && name.endsWith(nameSuffix));
		};
	}
	
	public FileConfiguration getConfig() {
		return config;
	}
	
	public String getMessage(String message, Object ... parameter) {
		return messages.getMessage(message, parameter);
	}
	
	public Map<String, ConfigurationSection> getFeatures() {
		return features;
	}
	
	public ConfigurationSection getFeature(String name) {
		return features.get(name);
	}
}